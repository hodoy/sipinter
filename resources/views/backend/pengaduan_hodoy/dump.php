<table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe">
											<tr>
												<td width="20%" bgcolor="grey"><strong><font color="white">Nama Lengkap :</font></strong></td>
												<td width="30%"><input type="text" name="nama_lengkap" class="form-control" value="{{ $pengaduan->nama_pengadu }}"></td>
												<td width="20%" bgcolor="grey"><strong><font color="white">Instansi :</font></strong></td>
												<td width="30%"><input type="text" name="instansi" class="form-control" value="{{ $pengaduan->instansi }}"></td>
											</tr>
											<tr>
												<td bgcolor="grey"><strong><font color="white">Nomor Identitas ( KTP / SIM / PASSPORT )</font></strong></td>
												<td><input type="text" name="nik" class="form-control" value="{{ $pengaduan->nik }}"></td>
												<td bgcolor="grey"><strong><font color="white">Jabatan :</font></strong></td>
												<td><input type="text" name="jabatan" class="form-control" value="{{ $pengaduan->jabatan }}"></td>
											</tr>
											<tr>
												<td bgcolor="grey"><strong><font color="white">Alamat :</font></strong></td>
												<td><input type="text" name="alamat" class="form-control" value="{{ $pengaduan->alamat }}"></td>
												<td bgcolor="grey"><strong><font color="white">Jenis Pengaduan :</font></strong></td>
												<td><input type="text" name="jenis_pengaduan_id" class="form-control" value="{{ $pengaduan->jenis_pengaduan }}" readonly></td>
											</tr>
											<tr>
												<td bgcolor="grey"><strong><font color="white">Email :</font></strong></td>
												<td><input type="text" name="email" class="form-control" value="{{ $pengaduan->email }}"></td>
												<td bgcolor="grey"><strong><font color="white">No Telepon :</font></strong></td>
												<td><input type="text" name="no_tlp" class="form-control" value="{{ $pengaduan->no_tlp }}"></td>
											</tr>
											<tr>
												<td><strong>Uraian Pengaduan :</strong></td>
												<td colspan="3">{{ $pengaduan->uraian }}</td>
												
											</tr>
											
										</table>