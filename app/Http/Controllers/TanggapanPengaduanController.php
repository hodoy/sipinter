<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Pengaduan;
use App\Models\PengaduanH;
use App\Models\JenisPengaduan;
use App\Models\BidangPengaduan;
use App\Models\TanggapanPengaduan;
use App\Models\LampiranPengaduan;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Auth;

class TanggapanPengaduanController extends Controller
{
	
	//jenis pengaduan
	public function index(){

        $bidang_pengaduan = DB::table('bidang_pengaduan')
                ->orderBy('id_bidang', 'asc')
                ->get();

        return view('backend/tanggapan_pengaduan/index',['bidang_pengaduan' => $bidang_pengaduan]);

    }
	
	public function datatables(Datatables $datatables)
    {

        DB::statement(DB::raw('set @rownum=0'));
        
        $query = TanggapanPengaduan::select("tanggapan_pengaduan.*", DB::raw('@rownum  := @rownum  + 1 AS rownum'))
		->get();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }
	
	public function getActionButton($post)
    {
		$viewButton = '<a href="' . url('/tanggapan-pengaduan/ubah_jenis/' . $post['id']) . '" class="btn btn-info">Ubah</a>&nbsp';
		$viewButton .= '<button class="btn waves-effect waves-light btn-danger" data-button="delete-button" title="Hapus" data-id="' . $post['id'] . '">Hapus</i></button>&nbsp';
        
        return $viewButton;
    }
	
	public function destroy($id)
    {
        $bidangpengaduan = BidangPengaduan::find($id);

        if (empty($bidangpengaduan)) {
            return response(['meta' => 400, 'message' => 'Failed Delete Data'], 400);
        }

        $bidangpengaduan->delete();

        return response(['meta' => 200, 'message' => 'Success Delete Data'], 200);

    }
	
	public function ubah_jenis($id)
    {
        $bidang_pengaduan = TanggapanPengaduan::find($id);
		return view('backend/tanggapan_pengaduan/ubah',['bidang_pengaduan' => $bidang_pengaduan]);
    }
	
	public function tambah()
    {
        return view('backend/tanggapan_pengaduan/tambah');
    }
	
	public function create(Request $request)
    {
        $bidangpengaduan = new TanggapanPengaduan();
        $bidangpengaduan->tanggapan = $request->tanggapan;

        $bidangpengaduan->save();

        return redirect('tanggapan-pengaduan')->with('saved', 'Data Berhasil Di Tambahkan');
    }
	
	public function update(Request $request, $id)
    {
        $bidang_pengaduan = TanggapanPengaduan::FindOrFail($id);
        $bidang_pengaduan->tanggapan = $request->tanggapan;
        $bidang_pengaduan->update();

        return redirect('tanggapan-pengaduan')->with('saved', 'Data Berhasil Di Update');
    }
}
