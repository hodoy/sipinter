<style type="text/css">
	html{
		padding: 0 0 0 0;
	}	

</style>
<img src="{{ asset('img/kop.png') }}" width="100%" />							

<center><h3>REKAP PENGADUAN PERIODE {{$d1}} s/d {{$d2}}</h3></center>
@if($type == 0)
<p align="center">SUMBER PENGADUAN : {{strtoupper($sumber)}}</p>
@endif
@if($type != 0)
<p align="center">SUMBER PENGADUAN : {{strtoupper($sumber->sumber_pengaduan)}}</p>
@endif
<table border="1" cellpadding="3" cellspacing="0" width="100%">
	<tr>
		<th align="center" width="5%">NO</th>
		<th align="center" width="20%">NAMA</th>
		<th align="center" width="15%">JENIS PENGADUAN</th>
		<th align="center" width="20%">URAIAN ADUAN</th>
		<th align="center" width="10%">SUMBER PENGADUAN</th>
		<th align="center" width="10%">STATUS</th>
	</tr>
	<?php foreach ($data as $key => $value): ?>
		<tr>
			<td align="center">{{$key+1}}</td>
			<td>{{strtoupper($value->nama_pengadu)}}</td>
			<td>{{strtoupper($value->jenis_pengaduan)}}</td>
			<td>{{$value->uraian}}</td>
			<td>{{strtoupper($value->sumber_pengaduan)}}</td>
			<td>{{strtoupper($value->status_flow)}}</td>
		</tr>
	<?php endforeach ?>
</table>