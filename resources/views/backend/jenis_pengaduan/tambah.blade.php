@extends('backend.main')
@section('content')
	<div class="container">
					
					<!-- Title -->
					<br>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">Tambah Jenis Pengaduan</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<p class="text-muted">Isi Formulir berikut dengan teliti <code>!!!</code></p>
										<div class="form-wrap mt-40">
											<form action="create" method="post" enctype="multipart/form-data">
											{{csrf_field()}}

											@if (session('saved'))
											<div class="alert alert-success">
											  <strong>Success!</strong>{{ session('saved') }}
											  <button type="button" class="close" data-dismiss="alert">x</button>
											</div>
											@endif
												<div class="row">
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('jenis_pengaduan') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Nama Jenis Pengaduan </label> 
															<input type="text" name="jenis_pengaduan" class="form-control" placeholder="Isi Nama Jenis Pengaduan">
														</div>	
													</div>
												</div><br>
												<button type="submit" class="btn btn-info">Submit</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
				
					<!-- Row -->
					<div class="row">
						<div class="col-md-6">
						</div>
						
						<div class="col-md-6">
						</div>
					</div>
					<!-- /Row -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
					<!-- /Row -->
					
				@endsection