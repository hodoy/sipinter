<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TindakLanjut extends Model
{
    protected $table = 'tindak_lanjut';
    protected $primaryKey = 'id';
    protected $fillable = [
	
        'pengaduan_id',
		'tindak_lanjut',
		'name',
    	
    ];
}
