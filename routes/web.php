<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend/index');
});
Route::get('/monitoring', function () {
    return view('frontend/monitoring');
});
Route::post('/detailmonitoring', function () {
    return view('frontend/detailmonitoring');
});
Route::get('/dashboard', 'PengaduanHController@indexgrafik');
Route::get('/hasildashboard', 'PengaduanHController@dashboard');
Auth::routes();
Route::get('/admin', 'PengaduanHController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');

//Route Pengaduan
Route::get('/pengaduan', 'PengaduanController@index');
Route::get('/pengaduan/data_pengaduan', 'PengaduanController@data_pengaduan');
Route::get('pengaduan/datatables', 'PengaduanController@datatables')->name('datatable/getdata');
Route::get('/pengaduan/datatablesadd', 'PengaduanController@datatablesadd');
Route::get('/pengaduan/create', 'PengaduanController@create');
Route::post('/pengaduan/create', 'PengaduanController@store');
Route::post('/pengaduan/upload_lampiran', 'PengaduanController@upload_lampiran');
Route::get('/pengaduan/show/{id}', 'PengaduanController@show');
Route::get('/pengaduan/{id}/edit', 'PengaduanController@edit');
Route::post('/pengaduan/{id}', 'PengaduanController@update');
Route::get('/pengaduan/sms', 'PengaduanController@dataSms');
Route::get('/pengaduan/webperizinan', 'PengaduanController@dataPengaduan');


//Route API
Route::get('/pengaduan/api_sms', 'PengaduanController@dataSms');
Route::get('/pengaduan/api_web', 'PengaduanController@dataPengaduan');
Route::get('/pengaduan/insert_api_web/{resi}/{nama_pemohon}/{jabatan}/{alamat_pemohon_jalan}/{nik_pemohon}/{telp_pemohon}/{email_pemohon}/{id_kategori_pengaduan}/{isi_pengaduan}', 'PengaduanController@insert_api_web');
Route::get('/pengaduan/insert_api_sms/{sendernumber}/{text}/{updatedindb}', 'PengaduanController@insert_api_sms');
Route::post('/insert_apisms/', 'PengaduanController@addnew');
Route::post('/insert_apiweb/', 'PengaduanController@addnewweb');

//Route Pengaduan Hodoy
//Route::get('/pengaduann', 'PengaduanHController@index');


Route::get('/pengaduann/data_pengaduan', 'PengaduanHController@data_pengaduan');
Route::get('pengaduann/datatables', 'PengaduanHController@datatables')->name('datatable/getdata');

Route::get('/pengaduann/data_pengaduan_selesai', 'PengaduanHController@data_pengaduan_selesai');
Route::get('pengaduann/datatables_selesai', 'PengaduanHController@datatables_selesai');

Route::get('/pengaduann/data_pengaduan_ditolak', 'PengaduanHController@data_pengaduan_ditolak');
Route::get('pengaduann/datatables_ditolak', 'PengaduanHController@datatables_ditolak');

Route::get('/pengaduann/data_pengaduan_proses', 'PengaduanHController@data_pengaduan_proses');
Route::get('pengaduann/datatables_proses', 'PengaduanHController@datatables_proses');


Route::get('/pengaduann/datatablesadd', 'PengaduanHController@datatablesadd');
Route::get('/pengaduann/create', 'PengaduanHController@create');
Route::post('/pengaduann/create', 'PengaduanHController@store');
Route::post('/pengaduann/disposisi', 'PengaduanHController@disposisi');
Route::post('/pengaduann/acc', 'PengaduanHController@acc');
Route::post('/pengaduann/tanggapan', 'PengaduanHController@tanggapan');
Route::post('/pengaduann/arsip', 'PengaduanHController@arsip');
Route::post('/pengaduann/upload_lampiran', 'PengaduanHController@upload_lampiran');

//REPORT
Route::get('/report', 'ReportController@index');
Route::get('/reporthasil', 'ReportController@report');


Route::get('/pengaduann/show/{id}', 'PengaduanHController@show');
Route::get('/pengaduann/{id}/edit', 'PengaduanHController@edit');
Route::post('/pengaduann/{id}', 'PengaduanHController@update');
//Route::delete('/pengaduann/{id}', 'PengaduanHController@destroy');
Route::get('/pengaduann/sms', 'PengaduanHController@dataSms');
Route::get('/pengaduann/webperizinan', 'PengaduanHController@dataPengaduan');

Route::get('/pengaduan/monitoring', 'PengaduanHController@monitorData');
Route::post('/pengaduan/updatedata/{id}', 'PengaduanHController@update');

//jenis pengaduan
Route::get('/jenis-pengaduan/', 'JenisPengaduanController@index');
Route::get('/jenis-pengaduan/datatables', 'JenisPengaduanController@datatables');
Route::get('/jenis-pengaduan/tambah', 'JenisPengaduanController@tambah');
Route::post('/jenis-pengaduan/create', 'JenisPengaduanController@create');
Route::get('/jenis-pengaduan/ubah_jenis/{id}', 'JenisPengaduanController@ubah_jenis');
Route::post('/jenis-pengaduan/update/{id}', 'JenisPengaduanController@update');
Route::delete('/jenis-pengaduan/{id}', 'JenisPengaduanController@destroy');

//bidang pengaduan
Route::get('/bidang-pengaduan/', 'BidangPengaduanController@index');
Route::get('/bidang-pengaduan/datatables', 'BidangPengaduanController@datatables');
Route::get('/bidang-pengaduan/tambah', 'BidangPengaduanController@tambah');
Route::post('/bidang-pengaduan/create', 'BidangPengaduanController@create');
Route::get('/bidang-pengaduan/ubah_jenis/{id}', 'BidangPengaduanController@ubah_jenis');
Route::post('/bidang-pengaduan/update/{id}', 'BidangPengaduanController@update');
Route::delete('/bidang-pengaduan/{id}', 'BidangPengaduanController@destroy');


//tanggapan pengaduan
Route::get('/tanggapan-pengaduan/', 'TanggapanPengaduanController@index');
Route::get('/tanggapan-pengaduan/datatables', 'TanggapanPengaduanController@datatables');
Route::get('/tanggapan-pengaduan/tambah', 'TanggapanPengaduanController@tambah');
Route::post('/tanggapan-pengaduan/create', 'TanggapanPengaduanController@create');
Route::get('/tanggapan-pengaduan/ubah_jenis/{id}', 'TanggapanPengaduanController@ubah_jenis');
Route::post('/tanggapan-pengaduan/update/{id}', 'TanggapanPengaduanController@update');
Route::delete('/tanggapan-pengaduan/{id}', 'TanggapanPengaduanController@destroy');

//sumber pengaduan
Route::get('/sumber-pengaduan/', 'SumberPengaduanController@index');
Route::get('/sumber-pengaduan/datatables', 'SumberPengaduanController@datatables');
Route::get('/sumber-pengaduan/tambah', 'SumberPengaduanController@tambah');
Route::post('/sumber-pengaduan/create', 'SumberPengaduanController@create');
Route::get('/sumber-pengaduan/ubah/{id}', 'SumberPengaduanController@ubah');
Route::post('/sumber-pengaduan/update/{id}', 'SumberPengaduanController@update');
Route::delete('/sumber-pengaduan/{id}', 'SumberPengaduanController@destroy');

Route::get('/testmail/', 'TestController@index');
Route::get('/testmail/getmail.aspx', 'TestController@getGmail');
Route::get('/testmail/getmaill', 'TestController@getMail');

