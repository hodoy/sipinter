<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogMonitor extends Model
{
    protected $table = 'log_pengaduan';
    protected $primaryKey = 'id';
    protected $fillable = [
	
		'pengaduan_id',
    	'proses_name',
        'next_task_name',
        'name',
        'catatan',
    	
    ];
}
