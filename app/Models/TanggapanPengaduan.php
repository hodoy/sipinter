<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TanggapanPengaduan extends Model
{
    protected $table = 'tanggapan_pengaduan';
    protected $primaryKey = 'id';
    protected $fillable = [
	
		'tanggapan',
    ];
}
