<?php

namespace App\Http\Requests\sumber_pengaduan;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sumber_pengaduan' => 'Required',
            'kode' => 'Required',
        ];
    }

    public function messages()
    {
        return [
            'sumber_pengaduan.required' => 'Sumber Pengaduan Tidak Boleh Kosong',
            'kode.required' => 'Kode Tidak Boleh Kosong',
        ];
    }
}