<!doctype html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<title>SI-PINTER DPMPTSP</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link href="https://fonts.googleapis.com/css?family=Montserrat:700%7CKarla:400,400i,700" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/base.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/mmenu.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/font-awesome.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/slick.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/magnific.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/jquery-ui-1.10.4.datepicker.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('frontend/style.css')}}">

		<link rel="icon" href="{{ asset('/img/logo.png') }}" type="image/x-icon">
		<link rel="apple-touch-icon" href="#">
		<link rel="apple-touch-icon" sizes="72x72" href="#">
		<link rel="apple-touch-icon" sizes="114x114" href="#">

	</head>
	<body>

		<div id="page">
			<header class="header header-sticky">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-4 col-xs-8">
							<h1 class="site-logo">
								<a href="">SI-PINTER</a>
							</h1>
							<!-- <p class="site-tagline">Premium Hotel Template</p> -->
						</div>

						<div class="col-lg-8 col-xs-4">
							<nav class="nav">
								<ul class="navigation-main">
									<li class="current-menu-item menu-item">
										<a href="{{ url('/') }}">Beranda</a>
										
									</li>
									<li class="menu-item">
										<a href="{{ url('/monitoring') }}">Monitoring Pengaduan</a>
										
									</li>
									
									<li class="nav-btn">
										<a href="{{ url('/login') }}">Login</a>
									</li>
								</ul>
								<!-- #navigation -->
							</nav>
							<!-- #nav -->

							<div id="mobilemenu"></div>
							<a href="#mobilemenu" class="mobile-nav-trigger">
								<i class="fa fa-navicon"></i> MENU
							</a>
						</div>
					</div>
				</div>
			</header>
			@yield('content')

			<footer class="footer">
				
					
						<div class="col-xs-12">
							
							
								<h4 class="site-logo-footer">SI - PINTER</h4>
								<p class="site-tagline">SISTEM INFORMASI PELAYANAN PENGADUAN TERPADU<br>DPMPTSP<br>KABUPATEN BANDUNG BARAT</p>

							
						</div>
					
				
			</footer>
		</div>

		<!-- Javascript
================================================== -->
		<script src="{{asset('frontend/js/jquery-1.12.3.min.js')}}"></script>
		<script src="{{asset('frontend/js/jquery-ui-1.10.4.custom.min.js')}}"></script>
		<script src="{{asset('frontend/js/jquery.mmenu.min.all.js')}}"></script>
		<script src="{{asset('frontend/js/slick.js')}}"></script>
		<script src="{{asset('frontend/js/jquery.sticky-kit.min.js')}}"></script>
		<script src="{{asset('frontend/js/jquery.fitvids.js')}}"></script>
		<script src="{{asset('frontend/js/jquery.magnific-popup.js')}}"></script>
		<script src="{{asset('frontend/js/jquery.matchHeight.js')}}"></script>
		<script src="{{asset('frontend/js/scripts.js')}}"></script>
		@stack('custom-scripts')
	  @section('js')
	  @show
	</body>
</html>