@extends('frontend.main')
@section('content')
<style>
	#kuy{
		padding : 5% 10% 0% 10%;
	}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

@csrf
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							
								<header class="entry-header">
									<h1><strong>MONITORING PENGADUAN #{{$data->kode_pengaduan}}</strong></h1>
									<br>
									<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">LIHAT TINDAK LANJUT</button>

									<div class="entry-meta">
										
									</div>
								</header>

								@if($monitor->count() == 0)
								<?php $ver = 0; ?>
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-proccess.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p align="center"><font color="white" size="3"><strong>VERIFIKASI DATA</strong></font></p>
													
												</div>
											</figure>
										</div>
										
									</div>
								@endif

								<div class="row row-items">
									<?php 
										$st = "";
										$no = 1;
										$cat = "";
										$date = ""; ?>

									@foreach($monitor as $key => $mon)
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-finish.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><font color="white" size="3"><strong><br>#{{$key+1}}<br>{{$mon->proses_name}}<br>({{$mon->name}})<br> {{$date}} s/d {{$mon->created_at}}<br>catatan : {{$mon->catatan}}</strong></font></p>
													
												</div>
											</figure>
										</div>
										
									</div>
									<?php $no++;
									$ver = 1;
									$date = $mon->created_at; 
									$cat = $mon->catatan;
									?>
									@endforeach
									@if($data->status_flow == "Proses" && $data->acc == 1 && $ver == 1)
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-proccess.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><font color="white" size="4"><strong><br>#{{$no}}<br>{{$data->proses_name}}<br>{{$date}}</strong></font></p>
													
												</div>
											</figure>
										</div>
									</div>	
									
									@endif
									@if($data->status_flow == "arsip" && $data->acc == 1)
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-finish.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><font color="white" size="3"><strong><br>#{{$no}}<br>ARSIP <br>Catatan : {{$mon->catatan}}</strong></font></p>
													
												</div>
											</figure>
										</div>
									</div>	
									</div>	
									@endif
									@if($data->status_flow == "arsip" && $data->acc == 0)
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-fail.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><font color="white" size="3"><strong><br>#{{$no}}<br>ARSIP<br>catatan : {{$cat}}</strong></font></p>
													
												</div>
											</figure>
										</div>
									</div>	
									</div>	
									@endif
										
									</div>
									
								</div>
						</div>
					</div>
				</div>

				<div id="kuy">
					<table border="1px solid black" width="300px">
						<tr>
							<td rowspan="4" align="center"><strong>STATUS</strong></td>
							<td align="center"><strong>WARNA</strong></td>
							<td align="center"><strong>KETERANGAN</strong></td>
						</tr>
						<tr>
							
							<td bgcolor="yellow"></td>
							<td align="center">STEP SELANJUTNYA</td>
						</tr>
						<tr>
							
							<td bgcolor="green"></td>
							<td align="center">SELESAI DI PROSES</td>
						</tr>
						<tr>
							
							<td bgcolor="red"></td>
							<td align="center">DITOLAK</td>
						</tr>
						
					</table>
					</div>
					<br>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><strong>DATA TINDAK LANJUT PENGADUAN #{{$data->kode_pengaduan}}</strong></h4>
      </div>
      <div class="modal-body">
        			<p><b>Uraian Pengaduan : </b><br>{{$data->uraian}}</p>
        			<hr>
					<table border="1px solid black" width="300px">
						<tr>
							<td width="15%" align="center"><strong>No</strong></td>
							<td align="center"><strong>Tindak Lanjut</strong></td>
							<td width="20%" align="center"><strong>Petugas</strong></td>
							
						</tr>
						@if($tindak_lanjut)
						<?php $n = 1; ?>
							@foreach($tindak_lanjut as $mon)
							<tr>
							
								<td width="15%" align="center">{{$n}}</td>
								<td align="center">{{$mon->tindak_lanjut}}</td>
								<td width="20%" align="center">{{$mon->name}}</td>
								
							</tr>
						<?php $n++; ?>
						@endforeach
						@endif
						@if($tindak_lanjut->count() == 0)
							<tr>
							
								<td width="15%" align="center" colspan="3">Belum Ada Data Tindak Lanjut Untuk Pengaduan Ini</td>
								
								
							</tr>
						@endif
						
					</table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>

  </div>
</div>

@endsection