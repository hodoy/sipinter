<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\PengaduanH;
use App\Models\LogMonitor;
use App\Models\Pengaduan;
use App\Models\SumberPengaduan;
use App\Models\TanggapanPengaduan;
use App\Models\TindakLanjut;
use App\Models\LampiranPengaduan;
use App\Http\Requests\pengaduan\StoreRequest;
use App\Http\Requests\pengaduan\UpdateRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Auth;
use DateTime;
use DateInterval;
use DatePeriod;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class PengaduanHController extends Controller
{
    public function index()
    {
        $data = [];
        $dataa = [];
        //echo "tes";die;
      
        $begin = new DateTime(date("Y-m-d"));
        $end = new DateTime(date("Y-m-d"));
        $begin->modify('-30 day');
        $end->modify('+1 day');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $sumber = SumberPengaduan::all();
        //$users = DB::table('pengaduan')->whereBetween('mulai', array("2018-09-10","2018-09-19"))->get();    
        //var_dump($users->count());die;
            foreach ($sumber as $key => $dt) {
                $tot = [];
                $date = [];
                foreach ($period as $key => $dta) {
                    $pengaduan = DB::table('pengaduan')->where('sumber_aduan_id','=',$dt->id_sumber_pengaduan)->where('mulai','like','%'.$dta->format("Y-m-d").'%')->get();
                    $jum = $pengaduan->count();
                    $tot[] = $jum;
                    $date[] = $dta->format("Y-m-d");   
                }
                $data['name'] = $dt->sumber_pengaduan;
                $data['data'] = $tot;
                $dataa[] = $data;
            }
            
        
        $a = json_encode($dataa);
        $b = json_encode($date);

        return view('backend/dashfront',['dataa' => $a,'date'=>$b,'begin'=>$begin,'end'=>$end]);
    }

    public function indexgrafik()
    {
        return view('backend/indexgrafik');
    }
    public function dashboard(Request $request)
    {   
        $data = [];
        $dataa = [];
        //echo "tes";die;
        $beginn = $request->a1;
        $endd = $request->a2;

        $begin = new DateTime($request->a1);
        $end = new DateTime($request->a2);
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $sumber = SumberPengaduan::all();
        //$users = DB::table('pengaduan')->whereBetween('mulai', array("2018-09-10","2018-09-19"))->get();    
        //var_dump($users->count());die;
            foreach ($sumber as $key => $dt) {
                $tot = [];
                $date = [];
                foreach ($period as $key => $dta) {
                    $pengaduan = DB::table('pengaduan')->where('sumber_aduan_id','=',$dt->id_sumber_pengaduan)->where('mulai','like','%'.$dta->format("Y-m-d").'%')->get();
                    $jum = $pengaduan->count();
                    $tot[] = $jum;
                    $date[] = $dta->format("Y-m-d");   
                }
                $data['name'] = $dt->sumber_pengaduan;
                $data['data'] = $tot;
                $dataa[] = $data;
            }
            
        
        $a = json_encode($dataa);
        $b = json_encode($date);

        
        //var_dump($b);die; 

        return view('backend/dashboard', ['dataa' => $a,'date'=>$b,'begin'=>$beginn,'end'=>$endd]);
    }
    
    public function data_pengaduan()
    {
        return view('backend/pengaduan_hodoy/data_pengaduan');
    }
    public function data_pengaduan_selesai()
    {
        return view('backend/pengaduan_hodoy/data_pengaduan_selesai');
    }
    public function data_pengaduan_proses()
    {
        return view('backend/pengaduan_hodoy/data_pengaduan_proses');
    }
    public function data_pengaduan_ditolak()
    {
        return view('backend/pengaduan_hodoy/data_pengaduan_ditolak');
    }

    public function datatablesadd(Datatables $datatables, Request $request)
    {

        
        
    }

    public function addnew(Request $request)
    {
        
    }

    public function getActionButtonAdd($post)
    {
        
    }

    public function datatables(Datatables $datatables)
    {

        //$query = DB::select(DB::raw("select * from surat_pengaduan"));
        
        $query = PengaduanH::join('sumber_pengaduan','pengaduan.sumber_aduan_id','=','sumber_pengaduan.id_sumber_pengaduan')->join('jenis_pengaduan','pengaduan.jenis_pengaduan_id','=','jenis_pengaduan.id_jenis_pengaduan')->where('proses_name','=',Auth::user()->role)->get();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }
    public function datatables_selesai(Datatables $datatables)
    {

        //$query = DB::select(DB::raw("select * from surat_pengaduan"));
        
        $query = PengaduanH::join('sumber_pengaduan','pengaduan.sumber_aduan_id','=','sumber_pengaduan.id_sumber_pengaduan')->join('jenis_pengaduan','pengaduan.jenis_pengaduan_id','=','jenis_pengaduan.id_jenis_pengaduan')->where('status_flow','=','arsip')->where('acc','=',1)->get();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }
    public function datatables_proses(Datatables $datatables)
    {

        //$query = DB::select(DB::raw("select * from surat_pengaduan"));
        
        $query = PengaduanH::join('sumber_pengaduan','pengaduan.sumber_aduan_id','=','sumber_pengaduan.id_sumber_pengaduan')->join('jenis_pengaduan','pengaduan.jenis_pengaduan_id','=','jenis_pengaduan.id_jenis_pengaduan')->where('status_flow','=','Proses')->get();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }
    public function datatables_ditolak(Datatables $datatables)
    {

        //$query = DB::select(DB::raw("select * from surat_pengaduan"));
        
         $query = PengaduanH::join('sumber_pengaduan','pengaduan.sumber_aduan_id','=','sumber_pengaduan.id_sumber_pengaduan')->join('jenis_pengaduan','pengaduan.jenis_pengaduan_id','=','jenis_pengaduan.id_jenis_pengaduan')->where('status_flow','=','arsip')->where('acc','=',0)->get();


        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }

    public function getActionButton($post)
    {
        $viewButton = '<a href="' . url('/pengaduann/show/' . $post['id']) . '" class="btn btn-sm btn-info" title="Detail">DETAIL</a>';
        
        return $viewButton;
    }

    public function create(Request $request)
    {
        
    }

    public function store(StoreRequest $request)
    {
        $Pengaduan = new Pengaduan();
        $Pengaduan->nama_lengkap = $request->nama_lengkap;
        $Pengaduan->jabatan = $request->jabatan;
        $Pengaduan->nama_perusahaan = $request->nama_perusahaan;
        $Pengaduan->nomor_identitas = $request->nomor_identitas;
        $Pengaduan->alamat = $request->alamat;
        $Pengaduan->nomor_telepon = $request->nomor_telepon;
        $Pengaduan->email = $request->email;
        $Pengaduan->jenis_izin = $request->jenis_izin;
        $Pengaduan->uraian_singkat = $request->uraian_singkat;
        $Pengaduan->tindak_lanjut = $request->tindak_lanjut;

        $Pengaduan->save();

        return redirect('pengaduan')->with('saved', 'Data Berhasil Di Tambahkan');
        
    }
    public function disposisi(Request $request)
    {
        $Pengaduan = PengaduanH::FindOrFail($request->idx);

        $Kod = DB::table('sumber_pengaduan')
                ->where('id_sumber_pengaduan','=',$Pengaduan->sumber_aduan_id)
                ->first();

        //var_dump($Kod);die;

        $Log = new LogMonitor();
        $Log->pengaduan_id = $request->idx;
        $Log->proses_name = Auth::user()->role;
        $Log->next_taskname = $request->disposisi;
        $Log->name = Auth::user()->name;
        $Log->catatan = $request->catatan;

        $Log->save();

        if ($Pengaduan->kode_pengaduan == "") {
            $Pengaduan->kode_pengaduan = $Kod->kode.$request->idx;
        }
        $Pengaduan->proses_name = $request->disposisi;

        $Pengaduan->update();


        return redirect('pengaduann/data_pengaduan')->with('saved', 'Data Berhasil Di Disposisi');
        
    }

    public function acc(Request $request)
    {
        $Pengaduan = PengaduanH::FindOrFail($request->idx);

        $Kod = DB::table('sumber_pengaduan')
                ->where('id_sumber_pengaduan','=',$Pengaduan->sumber_aduan_id)
                ->first();

        //var_dump($Kod);die;


        if ($Pengaduan->kode_pengaduan == "") {
            $Pengaduan->kode_pengaduan = $Kod->kode.$request->idx;
        }
        $Pengaduan->proses_name = "verifikasi";
        $Pengaduan->acc = 1;

        $pesan = "Kode Pengaduan Anda Adalah $Kod->kode$request->idx , Cek Status Pengaduan Anda Silahkan Akses Melalui http://localhost/sipinter/public/monitoring";
        $no = $Pengaduan->no_tlp;
        $id = $request->idx;
		
		//send email
        if ($Pengaduan->email != '' || $Pengaduan->email != '-'){
		$penerima = $Pengaduan->email;
		$mail = new PHPMailer(true); // notice the \  you have to use root namespace here
		try {
			$mail->isSMTP(); // tell to use smtp
			$mail->CharSet = "utf-8"; // set charset to utf8
			$mail->SMTPAuth = true;  // use smpt auth
			$mail->SMTPSecure = "ssl"; // or ssl
			$mail->Host = "smtp.gmail.com";
			$mail->Port = 465; // most likely something different for you. This is the mailtrap.io port i use for testing. 
			$mail->Username = "infopengaduan.dpmptspkbb@gmail.com";
			$mail->Password = "ADUAN911";
			$mail->setFrom("sipinter@bandungbaratkab.go.id", "Si Pinter");
			$mail->Subject = "Pengaduan Anda Telah Diterima";
			$mail->MsgHTML($pesan);
			$mail->addAddress($penerima);
			$mail->send();
    		} catch (phpmailerException $e) {
    			dd($e);
    		} catch (Exception $e) {
    			dd($e);
    		}
        }
		

        $Pengaduan->update();

        /*echo 'http://smsdpmptsp.bandungbaratkab.go.id/Outbox/SendSmsKodePengaduan?no='.$no.'&pesan='.$pesan.'&id='.$id;
        die;*/



        return redirect('http://perizinan-latihan.bandungbaratkab.go.id/smspengaduan/index.php?r=kategori/send&id='.$id.'&pesan='.$pesan.'&no='.$no);
        
    }

    public function arsip(Request $request)
    {
        $Pengaduan = PengaduanH::FindOrFail($request->idx);

        //var_dump($Kod);die;

        $Log = new LogMonitor();
        $Log->pengaduan_id = $request->idx;
        $Log->proses_name = Auth::user()->role;
        $Log->next_taskname = "arsip";
        $Log->name = Auth::user()->name;
        $Log->catatan = $request->catatan;

        $Log->save();

        $Pengaduan->proses_name = "arsip";
        $Pengaduan->status_flow = "arsip";

        $Pengaduan->update();


        return redirect('pengaduann/data_pengaduan')->with('saved', 'Data Berhasil Di Arsip');
        
    }
    public function tanggapan(Request $request)
    {
        //var_dump($request->tindak_lanjut);die;
        $idd = "";
        foreach ($request->tindak_lanjut as $key => $value) {
            
            $Tang = new TindakLanjut();
            $Tang->pengaduan_id = $request->idx[$key];
            $Tang->tindak_lanjut = $request->tindak_lanjut[$key];
            $Tang->name = Auth::user()->name;
            $Tang->save();

            $idd = $request->idx[$key];
        
        }
        
       

        
        return redirect('pengaduann/show/'.$idd)->with('saved', 'Tanggapan Berhasil Ditambahkan');
        
    }
    public function monitorData(Request $request)
    {
        //var_dump(Auth::user()->role);die;

        $data = PengaduanH::where('kode_pengaduan','=',$request->kode)->first();

        if (!$data) {
            echo "<script>alert('Kode pengaduan yang anda masukan tidak terdaftar pada database kami..');window.history.back();</script>";        
        }else{

        $monitordata = LogMonitor::where('pengaduan_id','=',$data->id)->get();
        $tindak_lanjut = TindakLanjut::where('pengaduan_id','=',$data->id)->get();

        //var_dump($monitordata);
        //$data = PengaduanH::join('lampiran_pengaduan','lampiran_pengaduan.surat_pengaduan_id','=','pengaduan.id')->where('kode_pengaduan','=',"1")->first();
        
        return view('frontend/detailmonitoring', ['data' => $data,'monitor'=>$monitordata,'tindak_lanjut'=>$tindak_lanjut]);
        
        }
    }

    public function show($id)
    {
		$pengaduan = PengaduanH::join('jenis_pengaduan','jenis_pengaduan.id_jenis_pengaduan','=','pengaduan.jenis_pengaduan_id')->where('id','=',$id)->first();

		//var_dump($pengaduan);die;

        $monitordata = LogMonitor::where('pengaduan_id','=',$id)->get();

        $tanggapan = TanggapanPengaduan::all();

        $tindak_lanjut = TindakLanjut::where('pengaduan_id','=',$id)->get();
		
		$lampiran = PengaduanH::join('lampiran_pengaduan','lampiran_pengaduan.surat_pengaduan_id','=','pengaduan.id')->where('id','=',$id)->get();

        //var_dump($lampiran);die;
		
		return view('backend/pengaduan_hodoy/detail_pengaduan', ['pengaduan' => $pengaduan,'tanggapan' => $tanggapan, 'lampiran' => $lampiran,'monitordata'=>$monitordata,'tindak_lanjut'=>$tindak_lanjut]);
    }

    public function upload_lampiran(Request $request)
    {
        if($request->hasFile('doc')){	 
			$allowedfileExtension=['pdf'];	 
			$files = $request->file('doc');
			 
			foreach($files as $file){ 
				$filename = $file->getClientOriginalName();			 
				$extension = $file->getClientOriginalExtension();
				$check=in_array($extension,$allowedfileExtension);

				if($check){ 
					foreach ($request->doc as $photo) {	 
						//$filename = $photo->store('photos'); 
						$filename = $file->store('doc', ['disk' => 'uploads']);

						$LampiranPengaduan = new LampiranPengaduan();
						$LampiranPengaduan->surat_pengaduan_id = $request->surat_pengaduan_id;
						$LampiranPengaduan->filename = $filename;
						
						$LampiranPengaduan->save();
					}
					return redirect('pengaduann/show/'.$request->surat_pengaduan_id.'')->with('saved', 'Data Berhasil Di Upload');
				}else {
					return redirect('pengaduann/show/'.$request->surat_pengaduan_id.'')->with('failed', 'Gagal Upload, Perhatikan Format File');
				} 
			} 
		}
    }
	
	public function edit($id)
    {
        
    }

    public function getprogram(Request $request)
    {
        
    }

    public function update(UpdateRequest $request, $id)
    {
        
        $data = PengaduanH::FindOrFail($id);
        
        $data->nama_pengadu = $request->nama_lengkap;
        $data->nik = $request->nik;
        $data->instansi = $request->instansi;
        $data->alamat = $request->alamat;
        $data->jabatan = $request->jabatan;
        $data->email = $request->email;
        $data->no_tlp = $request->no_tlp;

        $data->update();
        //return redirect('pengaduann/data_pengaduan')->with('saved', 'Data Berhasil Di Disposisi');
        return redirect('pengaduann/show/'.$id)->with('saved', 'Data Berhasil Di Update');
    }

    public function destroy($id)
    {
        

    }
    public function monitorFront(Request $request)
    {
       
    }
    
    public function dataSms(){
    	$url = "http://perizinan-latihan.bandungbaratkab.go.id/pengaduan/sms";
		$z = file_get_contents($url);
		$a = json_decode($z,true);
		$bgz = "BuGrPIS6ptBW49WLOxPrxX1Roh";
		
		$data=openssl_decrypt($a,"AES-128-ECB",$bgz);
		
		$val = json_decode($data,true);
		
		var_dump($val);
    }
    public function dataPengaduan(){
    	$url = "http://perizinan-latihan.bandungbaratkab.go.id/pengaduan";
		$z = file_get_contents($url);
		$a = json_decode($z,true);
		$bgz = "BuGrPIS6ptBW49WLOxPrxX1Roh";
		
		$data=openssl_decrypt($a,"AES-128-ECB",$bgz);
		
		$val = json_decode($data,true);
		
		var_dump($val);
    }
	
	function tes_email(){
		
			$mail = new PHPMailer(true); // notice the \  you have to use root namespace here
		try {
			$mail->isSMTP(); // tell to use smtp
			$mail->CharSet = "utf-8"; // set charset to utf8
			$mail->SMTPAuth = true;  // use smpt auth
			$mail->SMTPSecure = "ssl"; // or ssl
			$mail->Host = "smtp.gmail.com";
			$mail->Port = 465; // most likely something different for you. This is the mailtrap.io port i use for testing. 
			$mail->Username = "aldi.fabregas99@gmail.com";
			$mail->Password = "";
			$mail->setFrom("sipinter@bandungbaratkab.go.id", "Si Pinter");
			$mail->Subject = "Test";
			$mail->MsgHTML("Kode Pengaduan Anda Adalah SMS3 , Cek Status Pengaduan Anda Silahkan Akses Melalui http://localhost/sipinter/public/monitoring");
			$mail->addAddress("hadilazuardii@gmail.com");
			$mail->send();
		} catch (phpmailerException $e) {
			dd($e);
		} catch (Exception $e) {
			dd($e);
		}
		die('success');
		
	}
}
