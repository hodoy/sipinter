<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengaduan extends Model
{
    protected $table = 'surat_pengaduan';
    protected $primaryKey = 'id_surat_pengaduan';
    protected $fillable = [
	
		'nama_lengkap',
    	'jabatan',
    	'nama_perusahaan',
    	'alamat',
    	'nama_perusahaan',
    	'nomor_telepon',
    	'nomor_identitas',
    	'jenis_izin',
    	'email',
    	'uraian_singkat',
    	'tindak_lanjut',
    ];
}
