@extends('backend.main')
@section('content')
	<div class="container">

					<!-- Title -->
				

					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h4 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i><strong>DETAIL PENGADUAN #KODE</strong></h4>
									</div>
									<div class="clearfix"></div>
								</div>
								<br>
								<div class="col-md-12">
									<div class="col-md-9">
										<table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe">
											<tr>
												<td width="20%"><strong>Nama Lengkap :</strong></td>
												<td width="30%">{{ $pengaduan->nama_lengkap }}</td>
												<td width="20%"><strong>Perusahaan :</strong></td>
												<td width="30%">{{ $pengaduan->nama_perusahaan }}</td>
											</tr>
											<tr>
												<td><strong>Nomor Identitas</strong></td>
												<td>{{ $pengaduan->nomor_identitas }}</td>
												<td><strong>Jabatan :</strong></td>
												<td>{{ $pengaduan->jabatan }}</td>
											</tr>
											<tr>
												<td><strong>Alamat :</strong></td>
												<td>{{ $pengaduan->alamat }}</td>
												<td><strong>Jenis Pengaduan</strong></td>
												<td>{{ $pengaduan->jenis_izin }}</td>
											</tr>
											<tr>
												<td><strong>Email</strong></td>
												<td>{{ $pengaduan->email }}</td>
												<td><strong>Uraian Pengaduan :</strong></td>
												<td>{{ $pengaduan->uraian_singkat }}</td>
											</tr>
											<tr>
												<td><strong>Nomor Telepon</strong></td>
												<td>{{ $pengaduan->nomor_telepon }}</td>
												<td><strong>Tindak Lanjut :</strong></td>
												<td>{{ $pengaduan->tindak_lanjut }}</td>
											</tr>
											
										</table>
									</div>
									<div class="col-md-3">
										<div class="panel panel-default border-panel card-view">
										<form action="{{ url('pengaduan/upload_lampiran') }}" method="post" enctype="multipart/form-data">
											{{ csrf_field() }}
														
												@if (session('saved'))
													<div class="alert alert-success">
													  <strong>Success!</strong>{{ session('saved') }}
														  <button type="button" class="close" data-dismiss="alert">x</button>
														</div>
														@endif
												@if (session('failed'))
												<div class="alert alert-danger">
												  <strong>Failed!</strong>{{ session('failed') }}
												  <button type="button" class="close" data-dismiss="alert">x</button>
												</div>
												@endif
															
														<h6 class="panel-title txt-dark">File Upload (Opsional)</h6>
														<br>		
														<input type="file" name="photos[]" multiple><input type="hidden" name="surat_pengaduan_id" value="{{ $pengaduan->id_surat_pengaduan }}">
																		
															<br>
													   <button type="submit" class="btn btn-info btn-icon left-icon  mr-10"> <i class="zmdi zmdi-edit"></i> <span>Upload</span></button>
														<a href="{{ url('pengaduan/data_pengaduan') }}" class="btn btn-default">Cancel</a>
														<br><br>
															
										</form>
									</div>

									</div>

									
								</div>

								

								
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-wrap">
												<div class="col-md-12">
														<div class="seprator-block"></div>
															
															<h4 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i><strong>MONITORING DATA PENGADUAN</strong></h4>
															<hr class="light-grey-hr"/>	
															
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		<table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe">
																		
																		<tr>
																			<td width="50%"><strong>NAMA PROSES</strong></td>
																			<td width="50%">{Nama Proses}</td>
																			
																		</tr>
																		<tr>
																			
																			<td><strong>STATUS</strong></td>
																			<td>{STATUS PENGADUAN}</td>
																		</tr>
																		
																		
																		
																	</table>
																		
																	</div>
																</div>
															</div>
															
														</div>
														
												</div>
											</div>
										</div>
									</div>
								

								


								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-wrap">
												<div class="col-md-12">
														<div class="seprator-block"></div>
															
															<h4 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i><strong>DATA LAMPIRAN</strong></h4>
															<hr class="light-grey-hr"/>	
															@foreach($lampiran as $lam)
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		
																		<div class="col-md-12">
																			<embed src="/pengaduan/storage/app/{{$lam->filename}}" width="100%" height="700px" />
																		</div>
																	</div>
																</div>
															</div>
															@endforeach
														</div>
														
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-6">
						</div>

						<div class="col-md-6">
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
					<!-- /Row -->

				@endsection
