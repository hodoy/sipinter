<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Pengaduan;
use App\Models\PengaduanH;
use App\Models\JenisPengaduan;
use App\Models\BidangPengaduan;
use App\Models\LampiranPengaduan;
use App\Http\Requests\bidang_pengaduan\StoreRequest;
use App\Http\Requests\bidang_pengaduan\UpdateRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Auth;

class BidangPengaduanController extends Controller
{
	
	//jenis pengaduan
	public function index(){

        $bidang_pengaduan = DB::table('bidang_pengaduan')
                ->orderBy('id_bidang', 'asc')
                ->get();

        return view('backend/bidang_pengaduan/index',['bidang_pengaduan' => $bidang_pengaduan]);

    }
	
	public function datatables(Datatables $datatables)
    {

        DB::statement(DB::raw('set @rownum=0'));
        
        $query = BidangPengaduan::select("bidang_pengaduan.*", DB::raw('@rownum  := @rownum  + 1 AS rownum'))
		->get();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }
	
	public function getActionButton($post)
    {
		$viewButton = '<a href="' . url('/bidang-pengaduan/ubah_jenis/' . $post['id_bidang']) . '" class="btn btn-info">Ubah</a>&nbsp';
		$viewButton .= '<button class="btn waves-effect waves-light btn-danger" data-button="delete-button" title="Hapus" data-id="' . $post['id_bidang'] . '">Hapus</i></button>&nbsp';
        
        return $viewButton;
    }
	
	public function destroy($id)
    {
        $bidangpengaduan = BidangPengaduan::find($id);

        if (empty($bidangpengaduan)) {
            return response(['meta' => 400, 'message' => 'Failed Delete Data'], 400);
        }

        $bidangpengaduan->delete();

        return response(['meta' => 200, 'message' => 'Success Delete Data'], 200);

    }
	
	public function ubah_jenis($id)
    {
        $bidang_pengaduan = BidangPengaduan::find($id);
		return view('backend/bidang_pengaduan/ubah',['bidang_pengaduan' => $bidang_pengaduan]);
    }
	
	public function tambah()
    {
        return view('backend/bidang_pengaduan/tambah');
    }
	
	public function create(StoreRequest $request)
    {
        $bidangpengaduan = new BidangPengaduan();
        $bidangpengaduan->nama_bidang = $request->nama_bidang;

        $bidangpengaduan->save();

        return redirect('bidang-pengaduan')->with('saved', 'Data Berhasil Di Tambahkan');
    }
	
	public function update(UpdateRequest $request, $id)
    {
        $bidang_pengaduan = BidangPengaduan::FindOrFail($id);
        $bidang_pengaduan->nama_bidang = $request->nama_bidang;
        $bidang_pengaduan->update();

        return redirect('bidang-pengaduan')->with('saved', 'Data Berhasil Di Update');
    }
}
