@extends('backend.main')
@section('content')
	<div class="container">

					<!-- Title -->
					<br>
					<!-- /Title -->

					<!-- Row -->
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">DATA PENGADUAN SELESAI DI PROSES</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								@if (session('saved'))
											<div class="alert alert-success">
											  <font color="white"><strong>Success!</strong>{{ session('saved') }}</font>
											  <button type="button" class="close" data-dismiss="alert">x</button>
											</div>
											@endif
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="table-wrap">
											<div class="table-responsive">
												<table id="data_pengaduan" class="table table-hover display  pb-30" data-tables="true">
												<thead>
													<tr>
														
														<th>KODE</th>
														<th>NAMA PENGADU</th>
														<th>INSTANSI</th>
														<th>JENIS ADUAN</th>
														<th>SUMBER ADUAN</th>
														<th>AKSI</th>
													</tr>
												</thead>
												<tbody>
											</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-6">
						</div>

						<div class="col-md-6">
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
					<!-- /Row -->

				@endsection

@push('custom-scripts')
    <script>
         $(function () {
            

            function loadData() {
                $('#data_pengaduan').DataTable({
                    destroy: true,
                    serverSide: true,
                    processing: true,
                    ajax: "{{url('/pengaduann/datatables_selesai/')}}",
                    columns: [
                    	
                        {data: 'kode_pengaduan'},
                        {data: 'nama_pengadu'},
                        {data: 'instansi'},
                        {data: 'jenis_pengaduan'},
                        {data: 'sumber_pengaduan'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}

                    ],
                    dom: "<'row'<'col-sm-6'i><'col-sm-6'f>><'table-responsive'tr><'row'<'col-sm-6'l><'col-sm-6'p>>",
                    language: {
                        paginate: {
                            previous: "&laquo;",
                            next: "&raquo;"
                        },
                        search: "_INPUT_",
                        searchPlaceholder: "Search"
                    }
                });
            }

            
            loadData();
        });


    </script>
@endpush
