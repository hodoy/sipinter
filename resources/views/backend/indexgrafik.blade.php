@extends('backend.main2')
@section('content')
<div class="container">
                    
                    <!-- Title -->
                    <br>
                    <!-- /Title -->
                    
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-panel card-view">
                                <div class="panel-heading">
                                    <div>
                                        <center><h3 class="panel-title txt-dark">GRAFIK PENGADUAN</h3></center>
                                    </div>
                                    
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        
                                        <div class="form-wrap mt-40">
                                            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                                            
                                            <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
                                            <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

                                           
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10">Periode Awal *</label> 
                                                            <input type="text" id="datepicker" name="awal" class="form-control" placeholder="yyyy-mm-dd">
                                                        </div>  
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10">Periode Akhir *</label> 
                                                            <input type="text" id="datepicker2" name="akhir" class="form-control" placeholder="yyyy-mm-dd">
                                                        </div>  
                                                    </div>
                                                    <center>
                                                    <button type="submit" class="btn btn-success" id="btn-monitor">MUAT</button>
                                                    </center>
                                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <script>
        $('#datepicker').datepicker({format: 'yyyy-mm-dd'});
        $('#datepicker2').datepicker({format: 'yyyy-mm-dd'});
        $(function () {
           
			function checkData(){
                var awal  = $('input[name="awal"]').val()
                var akhir  = $('input[name="akhir"]').val()
                 if(awal == '' || akhir ==''){
                    alert('Silahkan Pilih Periode Awal Dan Akhir Terlebih Dahulu !!!');
                }else{
          			window.location = "{!!URL::to('/hasildashboard')!!}" + "?a1=" + awal + "&a2=" +akhir ;
          		}
                
            }
            
            $('#btn-monitor').click(function(){
                checkData();
            });
            
        });
    </script>
@endsection