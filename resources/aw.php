@extends('backend.main')
@section('content')
<style type="text/css">
	#container {
  height: 100%;
  margin: 0 auto
}
</style>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<br>

<div id="container"></div>

<?php 
		echo $dataa;
		$tes = " [{
		    name: 'SMS',
		    data: [43934, 52503, 57177, 0, 0, 0, 0, 0]
		  }, {
		    name: 'EMAIL',
		    data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
		  }, {
		    name: 'SURAT',
		    data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
		  }, {
		    name: 'TELEPON',
		    data: [null, 0, 7988, 12169, 15112, 22452, 34400, 34227]
		  }]"; 
		  
?>
<script type="text/javascript">
	Highcharts.chart('container', {

  title: {
    text: 'GRAFIK PENGAJUAN PENGADUAN PERIODE'
  },

  subtitle: {
    text: ''
  },

  yAxis: {
    title: {
      text: 'Jumlah Keluhan'
    }
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle'
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: false
      },
      pointStart: 1
    }
  },

  series: <?php echo $dataa; ?>,

  responsive: {
    rules: [{
      condition: {
        maxWidth: 500
      },
      chartOptions: {
        legend: {
          layout: 'horizontal',
          align: 'center',
          verticalAlign: 'bottom'
        }
      }
    }]
  }

});
</script>
@endsection