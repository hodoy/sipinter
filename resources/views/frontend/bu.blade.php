@extends('frontend.main')
@section('content')
<style>
	#kuy{
		padding : 5% 10% 0% 10%;
	}
</style>
<main class="main">
@csrf
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<article class="entry">
								<header class="entry-header">
									<h1 class="entry-title">MONITORING PENGADUAN #{{$data->kode_pengaduan}}</h1>

									<div class="entry-meta">
										
									</div>
								</header>

								@if($monitor->count() == 0)
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-proccess.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><font color="white" size="4"><strong>VERIFIKASI DATA</strong></font></p>
													
												</div>
											</figure>
										</div>
										
									</div>
								@endif

								<div class="row row-items">
									<?php 
										$st = "";
										$no = 1;
										$date = ""; ?>

									@foreach($monitor as $key => $mon)
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-finish.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><font color="white" size="4"><strong><br>#{{$key+1}}<br>{{$mon->proses_name}}<br>({{$mon->name}})<br> {{$date}} s/d {{$mon->created_at}}</strong></font></p>
													<a href="{{ url('/') }}" class="btn btn-sm item-btn">DETAIL</a>
												</div>
											</figure>
										</div>
										
									</div>
									<?php $no++;
									$date = $mon->created_at; ?>
									@endforeach
									@if($data->status_flow == "Proses")
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-proccess.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><font color="white" size="4"><strong><br>#{{$no}}<br>{{$data->proses_name}}<br>{{$date}}</strong></font></p>
													
												</div>
											</figure>
										</div>
									@endif
									@if($data->status_flow == "arsip")
									<div class="col-xl-3 col-lg-4 col-xs-6">
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-finish.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><font color="white" size="4"><strong><br>#{{$no}}<br>{{$data->proses_name}}</strong></font></p>
													<a href="{{ url('/') }}" class="btn btn-sm item-btn">DETAIL</a>
												</div>
											</figure>
										</div>
									</div>	
										<div class="col-xl-3 col-lg-4 col-xs-6">
										
										<div class="item">
											<figure class="item-thumb">
												<a>
													<img src="{{ asset('img/bg-finish.png') }}" alt="" width="100%">
												</a>

												<div class="item-info">
													<p><br>#{{$no+1}}<br>PENGADUAN DI ARSIP</p>
													<a href="{{ url('/') }}" class="btn btn-sm item-btn">DETAIL</a>
												</div>
											</figure>
										</div>
									</div>	
									@endif
										
									</div>
									
								</div>
						</div>
					</div>
				</div>
				<div id="kuy">
			
			<table border="1px solid black" width="300px">
				<tr>
					<td rowspan="4" align="center"><strong>STATUS</strong></td>
					<td align="center"><strong>WARNA</strong></td>
					<td align="center"><strong>KETERANGAN</strong></td>
				</tr>
				<tr>
					
					<td bgcolor="yellow"></td>
					<td align="center">STEP SELANJUTNYA</td>
				</tr>
				<tr>
					
					<td bgcolor="green"></td>
					<td align="center">SELESAI DI PROSES</td>
				</tr>
				<tr>
					
					<td bgcolor="red"></td>
					<td align="center">DATA TIDAK LENGKAP</td>
				</tr>
				
			</table>
			</div>
			</main>

@endsection