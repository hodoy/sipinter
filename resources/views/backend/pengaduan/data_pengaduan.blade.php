@extends('backend.main')
@section('content')
	<div class="container">

					<!-- Title -->
					<div class="row heading-bg">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<h5 class="txt-dark"></h5>
						</div>


					</div>
					<!-- /Title -->

					<!-- Row -->
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">data Table</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="table-wrap">
											<div class="table-responsive">
												<table id="data_pengaduan" class="table table-hover display  pb-30" data-tables="true">
												<thead>
													<tr>
														<th>Nama</th>
														<th>Jabatan</th>
														<th>Nama Perusahaan</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
											</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-6">
						</div>

						<div class="col-md-6">
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
					<!-- /Row -->

				@endsection

@push('custom-scripts')
    <script>
         $(function () {
            $('[data-tables=true]').on('click', '[data-button=delete-button]', function (e) {
                if (confirm('You are about to delete this row, are you sure ?')) {
                    $.ajax({
                        url: "{{ url("simral-program")  }}/delete/" + $(this).attr('data-id'),
                        type: "POST",
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (res) {
                            loadData();
                        },
						error: function (xhr, ajaxOptions, thrownError) {
							alert(xhr.status);
							alert(thrownError);
						}
                    })
                }
            });

            function loadData() {
                $('#data_pengaduan').DataTable({
                    destroy: true,
                    serverSide: true,
                    processing: true,
                    ajax: "{{url('/pengaduan/datatables/')}}",
                    columns: [
                        {data: 'nama_lengkap'},
                        {data: 'jabatan'},
                        {data: 'nama_perusahaan'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}

                    ],
                    dom: "<'row'<'col-sm-6'i><'col-sm-6'f>><'table-responsive'tr><'row'<'col-sm-6'l><'col-sm-6'p>>",
                    language: {
                        paginate: {
                            previous: "&laquo;",
                            next: "&raquo;"
                        },
                        search: "_INPUT_",
                        searchPlaceholder: "Search"
                    }
                });
            }

            $('#sel_simral_urusan').change(function() {
                getData = document.getElementById("sel_simral_urusan");
                strBidang = getData.options[getData.selectedIndex].value;
                loadData();
            });

            $('#btn-tambah').click(function(){
                getData = document.getElementById("sel_simral_urusan");
                strBidang = getData.options[getData.selectedIndex].value;
                window.location = "{!!URL::to('simral-program/create')!!}" + "/" + strBidang;
            });
            loadData();
        });


    </script>
@endpush
