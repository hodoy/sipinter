<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>LOGIN PAGE ADMIN</title>
		<meta name="description" content="Admintres is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Admintres Admin, Admintresadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		<!-- Bootstrap Colorpicker CSS -->
		<link href="{{ asset('backend/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/>
		
		<!-- select2 CSS -->
		<link href="{{ asset('backend/vendors/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
		
		<!-- switchery CSS -->
		<link href="{{ asset('backend/vendors/bower_components/switchery/dist/switchery.min.css') }}" rel="stylesheet" type="text/css"/>
		
		<!-- bootstrap-select CSS -->
		<link href="{{ asset('backend/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
		
		<!-- bootstrap-tagsinput CSS -->
		<link href="{{ asset('backend/vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"/>
		
		<!-- bootstrap-touchspin CSS -->
		<link href="{{ asset('backend/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css"/>
		
		<!-- multi-select CSS -->
		<link href="{{ asset('backend/vendors/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css"/>
		
		<!-- Bootstrap Switches CSS -->
		<link href="{{ asset('backend/vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
		
		<!-- Bootstrap Datetimepicker CSS -->
		<link href="{{ asset('backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css"/>
		<link href="{{ asset('backend/vendors/bower_components/owl.carousel/dist/assets/owl.carousel.min.css') }}" rel="stylesheet" type="text/css"/>
		<link href="{{ asset('backend/vendors/bower_components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet" type="text/css"/>
		
		
		<!-- Custom CSS -->
		<link href="{{ asset('backend/dist/css/style.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('backend/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper  pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="#">
								<img src="{{ asset('/img/logo.png') }}" width="100px" />
								
							</a>
				</div>
				
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float card-view pt-30 pb-30">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10"><strong>LOGIN ADMIN</strong></h3>
											<h6 class="text-center nonecase-font txt-grey">Silahkan masukan email dan password anda</h6>
										</div>	
										<div class="form-wrap">
											<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
											@csrf
											@if ($errors->has('email'))
														<span class="invalid-feedback" role="alert">
															<font style="color : red"><strong>Username atau Password Yang Anda Masukan Salah</strong></font>
														</span>
													@endif
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputEmail_2">Username</label>
													<input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password</label>
													
													<div class="clearfix"></div>
													 <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

													@if ($errors->has('password'))
														<span class="invalid-feedback" role="alert">
															<strong>{{ $errors->first('password') }}</strong>
														</span>
													@endif
												</div>
												
												<div class="form-group">
													<div class="checkbox checkbox-primary pr-10 pull-left">
														<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

														<label class="form-check-label" for="remember">
															{{ __('Ingat username anda ?') }}
														</label>
														<!--<a class="btn btn-link" href="{{ route('password.request') }}">
													{{ __('Lupa Password?') }}-->
												</a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="form-group text-center">
												<button type="submit" class="btn btn-orange btn-rounded">
													{{ __('Login') }}
												</button>
												<br>
												
													
												</div>
											</form>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		
		<!-- jQuery -->
		<script src="{{ asset('backend/vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="{{ asset('/backend/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
		
		<!-- Moment JavaScript -->
		<script type="text/javascript" src="{{ asset('backend/vendors/bower_components/moment/min/moment-with-locales.min.js') }}"></script>
		
		<!-- Bootstrap Colorpicker JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
		
		<!-- Switchery JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/switchery/dist/switchery.min.js') }}"></script>
		
		<!-- Select2 JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
		
		<!-- Bootstrap Select JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
		
		<!-- Bootstrap Tagsinput JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
		
		<!-- Bootstrap Touchspin JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>
		
		<!-- Multiselect JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
		
		 
		<!-- Bootstrap Switch JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js') }}"></script>
		
		<!-- Bootstrap Datetimepicker JavaScript -->
		<script type="text/javascript" src="{{ asset('backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
		
		<!-- Form Advance Init JavaScript -->
		<script src="{{ asset('backend/dist/js/form-advance-data.js') }}"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="{{ asset('backend/dist/js/jquery.slimscroll.js') }}"></script>
	
		<!-- Fancy Dropdown JS -->
		<script src="{{ asset('backend/dist/js/dropdown-bootstrap-extended.js') }}"></script>
		
		<!-- Owl JavaScript -->
		<script src="{{ asset('backend/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js') }}"></script>
	
		<!-- Init JavaScript -->
		<script src="{{ asset('backend/dist/js/init.js') }}	"></script>
	</body>
</html>
