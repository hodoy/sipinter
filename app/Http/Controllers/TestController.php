<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Pengaduan;
use App\Models\PengaduanH;
use App\Models\JenisPengaduan;
use App\Models\BidangPengaduan;
use App\Models\LampiranPengaduan;
use App\Http\Requests\bidang_pengaduan\StoreRequest;
use App\Http\Requests\bidang_pengaduan\UpdateRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Auth;
use Google_Client;
use Google_Service_Gmail;

class TestController extends Controller
{
	
	//jenis pengaduan
	public function index(){
		echo 'bagong';
    }
	
	public function getClient()
	{
		$client = new Google_Client();
		$client->setApplicationName('Gmail API PHP Quickstart');
		$client->setScopes(Google_Service_Gmail::GMAIL_READONLY);
		$client->setAuthConfig('onjs/credentials.json');
		$client->setAccessType('offline');

		// Load previously authorized credentials from a file.
		$credentialsPath = 'onjs/token.json';
		if (file_exists($credentialsPath)) {
			$accessToken = json_decode(file_get_contents($credentialsPath), true);
		} else {
			// Request authorization from the user.
			$authUrl = $client->createAuthUrl();
			printf("Open the following link in your browser:\n%s\n", $authUrl);
			print 'Enter verification code: ';
			$authCode = trim(fgets(STDIN));

			// Exchange authorization code for an access token.
			$accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

			// Check to see if there was an error.
			if (array_key_exists('error', $accessToken)) {
				throw new Exception(join(', ', $accessToken));
			}

			// Store the credentials to disk.
			if (!file_exists(dirname($credentialsPath))) {
				mkdir(dirname($credentialsPath), 0700, true);
			}
			file_put_contents($credentialsPath, json_encode($accessToken));
			printf("Credentials saved to %s\n", $credentialsPath);
		}
		$client->setAccessToken($accessToken);

		// Refresh the token if it's expired.
		if ($client->isAccessTokenExpired()) {
			$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
		}
		return $client;
	}

	public function getGmail(){
		// Get the API client and construct the service object.
		$client = $this->getClient();
		$service = new Google_Service_Gmail($client);

		// Print the labels in the user's account.
		$user = 'me';
		$results = $service->users_labels->listUsersLabels($user);

		$optParams = [];
		$optParams['maxResults'] = 50;
		$optParams['labelIds'] = 'INBOX'; // Only show messages in Inbox
		$messages = $service->users_messages->listUsersMessages('me',$optParams);
		$list = $messages->getMessages();
		//echo count($list).'<br><br>';
		$no = 1;
		for($i = 0; $i < count($list); $i++){
			$messageId = $list[$i]->getId(); // Grab first Message

			$optParamsGet = [];
			$optParamsGet['format'] = 'full'; // Display message in payload
			$message = $service->users_messages->get('me',$messageId,$optParamsGet);
			$messagePayload = $message->getPayload();
			$headers = $message->getPayload()->getHeaders();
			$parts = $message->getPayload()->getParts();
			
			$decodedMessage = '';
			if(!empty($parts)){
				$body = $parts[0]['body'];
				$rawData = $body->data;
				$sanitizedData = strtr($rawData,'-_', '+/');
				$decodedMessage = base64_decode($sanitizedData);
			}
			
			print_r($decodedMessage);
			echo $no.'<br><br>';
			$no++;
		}
	}
	public function getMail()
{
    /* connect to gmail */
    $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
    $username = 'infopengaduan.dpmptspkbb@gmail.com';
    $password = 'ADUAN911';

    $inbox = imap_open($hostname, $username, $password) or die('Cannot connect: ' . imap_last_error());

    $emails = imap_search($inbox, 'ALL');

    if ($emails) {
        $output = '';
        $mails = array();

        rsort($emails);

        foreach ($emails as $email_number) {
            $header = imap_headerinfo($inbox, $email_number);
            $message = quoted_printable_decode (imap_fetchbody($inbox, $email_number, 1));

            $from = $header->from[0]->mailbox . "@" . $header->from[0]->host;
            $toaddress = $header->toaddress;
            if(imap_search($inbox, 'UNSEEN')){
                /*Store from and message body to database*/
                DB::table('email')->insert(['from'=>$from, 'body'=>$message]);
                return view('emails.display');
            }
            else{
                $data = Email::all();
                return view('emails.display',compact('data'));

            }
        }
    }
        imap_close($inbox);
}

public function showMail($id){

    // get the id
    $message = Email::findOrFail($id);
    $m = $message->body;
    // show the view and pass the nerd to it
    return view('emails.showmail',compact('m'));
}
}
