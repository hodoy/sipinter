@extends('backend.main')
@section('content')
	<div class="container">

					<!-- Title -->
					<div class="row heading-bg">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<h5 class="txt-dark"></h5>
						</div>


					</div>
					<!-- /Title -->

					<!-- Row -->
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">Data Bidang Pengaduan</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<br>
								@if (session('saved'))
								<div class="alert alert-success">
								  <strong>Success!</strong>{{ session('saved') }}
								  <button type="button" class="close" data-dismiss="alert">x</button>
								</div>
								@endif
								<center><a href="{{ url('bidang-pengaduan/tambah') }}" class="btn btn-default">Tambah Data</a></center>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="table-wrap">
											<div class="table-responsive">
												<table id="bidang_pengaduan" class="table table-hover display  pb-30" data-tables="true">
												<thead>
													<tr>
														<th width="10%">No</th>
														<th width="70%">Bidang Pengaduan</th>
														<th width="20%">Action</th>
													</tr>
												</thead>
												<tbody>
											</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-6">
						</div>

						<div class="col-md-6">
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
					<!-- /Row -->

				@endsection

@push('custom-scripts')
    <script>
         $(function () {
            $('[data-tables=true]').on('click', '[data-button=delete-button]', function (e) {
                if (confirm('You are about to delete this row, are you sure ?')) {
                    $.ajax({
                        url: "{{ url("bidang-pengaduan")  }}/" + $(this).attr('data-id'),
                        type: "DELETE",
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (res) {
                            loadData();
                        },
                       error: function (xhr, ajaxOptions, thrownError) {
						alert(xhr.status);
						alert(thrownError);
					  }
                    })
                }
            });

            function loadData() {
                $('#bidang_pengaduan').DataTable({
                    destroy: true,
                    serverSide: true,
                    processing: true,
                    ajax: "{{url('/bidang-pengaduan/datatables/')}}",
                    columns: [
                        {data: 'rownum', searchable: false},
                        {data: 'nama_bidang'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}

                    ],
                    dom: "<'row'<'col-sm-6'i><'col-sm-6'f>><'table-responsive'tr><'row'<'col-sm-6'l><'col-sm-6'p>>",
                    language: {
                        paginate: {
                            previous: "&laquo;",
                            next: "&raquo;"
                        },
                        search: "_INPUT_",
                        searchPlaceholder: "Search"
                    }
                });
            }

            loadData();
        });


    </script>
@endpush
