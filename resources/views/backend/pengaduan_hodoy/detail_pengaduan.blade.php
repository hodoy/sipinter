@extends('backend.main')
@section('content')
	<div class="container">

					<!-- Title -->
				<?php //var_dump($pengaduan); ?>

					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h4 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i><strong>
											

											
											DETAIL PENGADUAN #{{$pengaduan->kode_pengaduan}}</h4>
										@if(Auth::user()->role != 'kadis')
										@if($pengaduan->status_flow == "Proses" && $pengaduan->acc == "1")
										<button class="btn btn-success" data-toggle="modal" data-target="#modal-disposisi"><span class='fa fa-check'></span>DISPOSISI</button>
										<button class="btn btn-danger" data-toggle="modal" data-target="#modal-arsip"><span class='fa fa-exclamation'></span> ARSIP</button>
										@endif
										@if($pengaduan->status_flow == "Proses" && $pengaduan->acc == "0")
										<div class="col-md-4">
											<form action="{{ url('pengaduann/acc') }}" method="post" enctype="multipart/form-data">
											{{ csrf_field() }}
											<input type="text" name="idx" class="form-control" value="{{$pengaduan->id}}" style="display: none">
											<button class="btn btn-success" type="submit"><span class='fa fa-check'></span>ACC</button><?php echo " &nbsp&nbsp&nbsp "; ?>
										</form>
										</div>
										<div class="col-md-3">
											<button class="btn btn-danger" data-toggle="modal" data-target="#modal-arsip"><span class='fa fa-exclamation'></span> ARSIP</button>
										</div>
										
										@endif
										@endif
										
									</div>
									<div class="clearfix"></div>
								</div>
								<br>
								<div class="col-md-12">
									<div class="col-md-9">
										<form action="{{ url('/pengaduan/updatedata',$pengaduan->id) }}" method="post" enctype="multipart/form-data">
											{{csrf_field()}}
											
											@if (session('saved'))
											<div class="alert alert-success">
											  <font color="white"><strong>Success!</strong>{{ session('saved') }}</font>
											  <button type="button" class="close" data-dismiss="alert">x</button>
											</div>
											@endif
											<div class="col-md-12">
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">NAMA LENGKAP</label>
													
												</div>	
											</div>
											<div class="col-md-4">
												<div class="form-group">
													
													<input type="text" name="nama_lengkap" class="form-control" value="{{ $pengaduan->nama_pengadu }}">
												</div>	
											</div>
											<!-- end -->
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">INSTANSI</label>
													
												</div>	
											</div>
											<div class="col-md-4">
												<div class="form-group">
													
													<input type="text" name="instansi" class="form-control" value="{{ $pengaduan->instansi }}">
												</div>	
											</div>
											</div>
											<!-- end -->
											<div class="col-md-12">
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">NIK (KTP / SIM / PASSPORT)</label>
													
												</div>	
											</div>
											<div class="col-md-4">
												<div class="form-group">
													
													<input type="text" name="nik" class="form-control" value="{{ $pengaduan->nik }}">
												</div>	
											</div>
											<!-- end -->
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">JABATAN</label>
													
												</div>	
											</div>
											<div class="col-md-4">
												<div class="form-group">
													
													<input type="text" name="jabatan" class="form-control" value="{{ $pengaduan->jabatan }}">
												</div>	
											</div>
										</div>
											<!-- end -->
											<div class="col-md-12">
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">ALAMAT</label>
													
												</div>	
											</div>
											<div class="col-md-4">
												<div class="form-group">
													
													<input type="text" name="alamat" class="form-control" value="{{ $pengaduan->alamat }}">
												</div>	
											</div>
											<!-- end -->
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">JENIS PENGADUAN</label>
													
												</div>	
											</div>
											<div class="col-md-4">
												<div class="form-group">
													
													<input type="text" name="jenis_pengaduan_id" class="form-control" value="{{ $pengaduan->jenis_pengaduan }}" readonly>
												</div>	
											</div>
											</div>
											<!-- end -->
											<div class="col-md-12">
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">EMAIL</label>
													
												</div>	
											</div>
											<div class="col-md-4">
												<div class="form-group">
													
													<input type="text" name="email" class="form-control" value="{{ $pengaduan->email }}">
												</div>	
											</div>
											<!-- end -->
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">NO TELEPON</label>
													
												</div>	
											</div>
											<div class="col-md-4">
												<div class="form-group">
													
													<input type="text" name="no_tlp" class="form-control" value="{{ $pengaduan->no_tlp }}">
												</div>	
											</div>
										</div>
											<!-- end -->
											<div class="col-md-12">
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label">URAIAN</label>
													
												</div>	
											</div>
											<div class="col-md-10">
												<div class="form-group">
													<textarea  class="form-control" readonly>{{ $pengaduan->uraian }}</textarea>
													
												</div>	
											</div>
											
										</div>
											<!-- end -->

											
										<br>
										@if(Auth::user()->role != 'kadis')
										<button type="submit" class="btn waves-effect waves-light btn-success" value="save"><i
                                		class="icon icon-save">  <i class="zmdi zmdi-edit"></i> Update Data</i></button>
                                		@endif
										</form>
									</div>
									<div class="col-md-3">
										<div class="panel panel-default border-panel card-view">
										<form action="{{ url('pengaduann/upload_lampiran') }}" method="post" enctype="multipart/form-data">
											{{ csrf_field() }}
														
												
														@if (session('failed'))
														<div class="alert alert-danger">
														  <strong>Failed!</strong>{{ session('failed') }}
														  <button type="button" class="close" data-dismiss="alert">x</button>
														</div>
														@endif
															
														<h6 class="panel-title txt-dark">File Upload (Opsional)</h6>
														<br>		
														<input type="file" name="doc[]" multiple required><input type="hidden" name="surat_pengaduan_id" value="{{ $pengaduan->id }}">
																		
															<br>
														@if(Auth::user()->role != 'kadis')
													   <button type="submit" class="btn btn-success btn-icon left-icon  mr-10"> <i class="zmdi zmdi-edit"></i> <span>Upload</span></button>
													   @endif
														
														
														<br><br># Data Lampiran Yang Telah Di Upload Dapat Dilihat Pada Bagian Bawah Halaman. <br><br>
															
										</form>
									</div>

									</div>

									
								</div>

									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-wrap">
												<div class="col-md-12">
														<div class="seprator-block"></div>
															
															<h4 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i><strong>MONITORING DATA PENGADUAN & TINDAKAN</strong></h4>
															<hr class="grey"/>	
															
															<div class="row">
																<div class="col-md-6">
																	<center><h4><strong>MONITORING DATA</strong></h4></center>
																	<div class="form-group">
																		<table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe">
																		@if($monitordata->count() == 0)
																			<tr bgcolor="yellow">
																				<td align="center" >Verifikasi</td>
																				<td align="center" ><?php
																							$start_date = new DateTime($pengaduan->mulai);
																							$since_start = $start_date->diff(new DateTime(date("Y-m-d H:i:s")));
																							if ($since_start->m) {
																								echo $since_start->m.' Bulan ';
																							}
																							if ($since_start->d) {
																								echo $since_start->d.' Hari ';
																							}
																							if ($since_start->h) {
																								echo $since_start->h.' Jam ';
																							}
																							if ($since_start->i) {
																								echo $since_start->i.' Menit ';
																							}
																							if ($since_start->s) {
																								echo $since_start->s.' Detik ';
																							}
																							
																							?>
																							
																				</td>
																			</tr>
																		@endif

																		@if($monitordata->count() > 0)
																			<tr>
																				<?php 
																				$st = $pengaduan->mulai;
																				$next = "";
																				$date = ""; 
																				
																				?>

																				@foreach($monitordata as $mon)
																				<?php 
																				$start_date = new DateTime($st);
																				$since_start = $start_date->diff(new DateTime($mon->created_at)); 
																				?>
																				<td bgcolor="green" align="center"><font color="white">{{$mon->proses_name}}<br>({{$mon->name}})<br>{{$st}} s/d {{$mon->created_at}}<br><?php if ($since_start->m) {
																								echo $since_start->m.' Bulan ';
																							}
																							if ($since_start->d) {
																								echo $since_start->d.' Hari ';
																							}
																							if ($since_start->h) {
																								echo $since_start->h.' Jam ';
																							}
																							if ($since_start->i) {
																								echo $since_start->i.' Menit ';
																							}
																							if ($since_start->s) {
																								echo $since_start->s.' Detik ';
																							}
																							
																							?><br>Catatan : {{$mon->catatan}}</font></td>
																				<?php 
																				$date = $mon->created_at;
																				$st = $mon->created_at;
																				$next = $mon->next_taskname;
																				 ?>
																				@endforeach
																				@if($pengaduan->status_flow == "Proses")<td bgcolor="yellow" align="center">{{$next}}<br>{{$date}}</td>
																				@endif
																				<?php $start_date = new DateTime($pengaduan->mulai);
																				$since_start = $start_date->diff(new DateTime($st));  ?>
																				<td bgcolor="grey" align="center"><font color="white"><br>Total Waktu : <br><?php if ($since_start->m) {
																								echo $since_start->m.' Bulan ';
																							}
																							if ($since_start->d) {
																								echo $since_start->d.' Hari ';
																							}
																							if ($since_start->h) {
																								echo $since_start->h.' Jam ';
																							}
																							if ($since_start->i) {
																								echo $since_start->i.' Menit ';
																							}
																							if ($since_start->s) {
																								echo $since_start->s.' Detik ';
																							}
																							
																							?></font></td>
																			</tr>
																		@endif
																						
																		</table>
																		
																	</div>
																</div>
																<div class="col-md-6">
																	<center><h4><strong>TINDAK LANJUT</strong></h4></center>
																	<div class="form-group">
																		<table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe">
																		@if($tindak_lanjut->count() == 0)
																			<tr>
																				<td align="center">Belum Ada Data Tindak Lanjut</td>
																			</tr>
																		@endif

																		@if($tindak_lanjut->count() > 0)
																			<?php $n = 1; ?>
																				@foreach($tindak_lanjut as $mon)
																				<tr>
																				
																					<td width="15%">{{$n}}</td>
																					<td>{{$mon->tindak_lanjut}}</td>
																					<td width="20%">{{$mon->name}}</td>
																					
																				</tr>
																			<?php $n++; ?>
																				@endforeach
																		@endif
																						
																		</table>
																		<br>
																		@if(Auth::user()->role != 'kadis')
																		@if($pengaduan->status_flow == "Proses" && $pengaduan->acc == "1")
																			<button class="btn btn-success" data-toggle="modal" data-target="#modal-tanggapan"><span class='fa fa-edit'></span> TAMBAH TINDAK LANJUT</button>
																		@endif
																		@endif
																		
																	</div>
																</div>
															</div>
															
														</div>
														
												</div>
											</div>
										</div>
									</div>
								

								


								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-wrap">
												<div class="col-md-12">
														<div class="seprator-block"></div>
															
															<h4 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i><strong>DATA LAMPIRAN</strong></h4>
															<hr class="grey"/>	

															<?php //var_dump($lampiran); ?>
															@foreach($lampiran as $lam)
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		
																		<div class="col-md-12">
																			<embed src="{{ asset('uploads/'.$lam->filename) }}" width="100%" height="700px" />
																		</div>
																	</div>
																</div>
															</div>
															@endforeach
														</div>
														
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
					
	<div class="modal fade" id="modal-disposisi">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h4 class="modal-title">DISPOSISI PENGADUAN</h4>
				</div>
				<div class="modal-body">
							<!-- form -->
								<form action="{{ url('pengaduann/disposisi')}}" method="post" enctype="multipart/form-data">
											{{csrf_field()}}

											
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label">Disposisi Ke Bagian </label> 
															<input type="text" name="idx" class="form-control" value="{{$pengaduan->id}}" style="display: none">
															<select name="disposisi" class="form-control">
																
																<option value="verifikasi">Verifikator</option>
																<option value="pokja">Pokja</option>
																
															</select>
														</div>	
													</div>
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label">Catatan Disposisi </label> 
															<textarea type="text" name="catatan" class="form-control" placeholder="Isi Catatan"></textarea>
														</div>	
													</div>
													
												</div>
												<button type="submit" class="btn btn-info">Submit</button>
											</form>
							<!-- end form -->
								</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal-arsip">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h4 class="modal-title">ARSIP PENGADUAN</h4>
				</div>
				<div class="modal-body">
							<!-- form -->
								<form action="{{ url('pengaduann/arsip')}}" method="post" enctype="multipart/form-data">
											{{csrf_field()}}

											
												<div class="row">
													
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label">Catatan Arsip </label> 
															<input type="text" name="idx" class="form-control" value="{{$pengaduan->id}}" style="display: none">
															<textarea type="text" name="catatan" class="form-control" placeholder="Isi Catatan"></textarea>
														</div>	
													</div>
													
												</div>
												<button type="submit" class="btn btn-info">Submit</button>
											</form>
							<!-- end form -->
								</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal-tanggapan">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h4 class="modal-title">TAMBAH TANGGAPAN PENGADUAN</h4>
				</div>
				<div class="modal-body">
							<!-- form -->
								<form action="{{ url('/pengaduann/tanggapan')}}" method="post" enctype="multipart/form-data">
											{{csrf_field()}}

												<div class="row">
													
													<div class="col-md-12">
														<div class="form-group">
															
															<div class="input_fields_wrap">
															    <center><button class="btn btn-success" id="add_field_button" align="right">Tambah Tanggapan Lain</button><br><br></center>
															    <label>Tanggapan</label>
															    <br>
															    <div>
															    	<input type="text" name="idx[]" class="form-control" value="{{$pengaduan->id}}" style="display: none">
															    	<div class="col-md-12">
															    		<select class="form-control" name="tindak_lanjut[]">
															    			<option value="0">Pilih Tanggapan</option>
															    			@foreach($tanggapan as $tang)
															    				<option value="{{$tang->tanggapan}}">{{$tang->tanggapan}}</option>
															    			@endforeach
															    		</select><br>
															    	</div>	

															    </div>

															</div> 
															
															
														</div>	
													</div>
													
												</div>
												<button type="submit" class="btn btn-success">Submit</button>
									</form>
							<!-- end form -->
								</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
@endsection
@push('custom-scripts')
<script>
	$(document).ready(function() {
    var max_fields      = 5; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $("#add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="idx[]" class="form-control" value="{{$pengaduan->id}}" style="display: none"><div class="col-md-12"><textarea type="text" name="tindak_lanjut[]" class="form-control" placeholder="Tanggapan"></textarea></div><a href="#" class="remove_field btn btn-danger" align="right">Hapus</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
@endpush
				
