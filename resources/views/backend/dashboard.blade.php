@extends('backend.main')
@section('content')
<style type="text/css">
	#container {
  height: 100%;
  width: 100%;
  margin: 0 auto
}
</style>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<br>

<div id="container"></div>

<?php 
		//echo $dataa;
		$tes = " [{
		    name: 'SMS',
		    data: [43934, 52503, 57177, 0, 0, 0, 0, 0]
		  }, {
		    name: 'EMAIL',
		    data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
		  }, {
		    name: 'SURAT',
		    data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
		  }, {
		    name: 'TELEPON',
		    data: [null, 0, 7988, 12169, 15112, 22452, 34400, 34227]
		  }]"; 
		  
?>
<script type="text/javascript">
	
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'JUMLAH PENGADUAN BERDASARKAN SUMBER PENGADUAN <br> PERIODE <?php echo $begin; ?> s/d <?php echo $end; ?>'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: <?php echo $date; ?>,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'JUMLAH PENGADUAN'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: <?php echo $dataa;?>
});
</script>
@endsection