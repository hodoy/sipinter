@extends('backend.main')
@section('content')
	<div class="container">
					
					<!-- Title -->
					<br>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">Formulir Isian Pengajuan</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<p class="text-muted">Isi Formulir berikut dengan teliti <br><code>Kolom Dengan Tanda * Harus Diisi</code></p>
										<div class="form-wrap mt-40">
											<form action="pengaduan/create" method="post" enctype="multipart/form-data">
											{{csrf_field()}}

											@if (session('saved'))
											<div class="alert alert-success">
											  <font color="white"><strong>Success!</strong>{{ session('saved') }}</font>
											  <button type="button" class="close" data-dismiss="alert">x</button>
											</div>
											@endif
												<div class="row">
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('nama_pengadu') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Nama Lengkap *</label> 
															<input type="text" name="nama_pengadu" class="form-control" placeholder="Isi Nama Lengkap">
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('instansi') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Instansi</label>
															<input type="text" name="instansi" class="form-control" placeholder="Instansi">
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('jabatan') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Jabatan</label>
															<input type="text" name="jabatan" class="form-control" placeholder="Jabatan">
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('nik') ? 'has-error' : '' }}">
															<label class="control-label mb-10">NIK ( KTP / SIM / PASSPORT )</label>
															<input type="text" name="nik" class="form-control" placeholder="Nomor Identitas KTP / SIM">
														</div>	
													</div>
													<div class="col-md-12">
														<div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Alamat</label>
															<textarea rows="5" name="alamat" class="form-control" placeholder="Alamat Perusahaan / Rumah"></textarea>
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('no_tlp') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Nomor Telepon *</label>
															<input type="number" name="no_tlp" class="form-control" placeholder="Nomor Telepon / HP">
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('jenis_pengaduan_id') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Bidang Pengaduan</label>
															<select name="bidang_pengaduan_id" class="form-control">
																@foreach($bidang_pengaduan as $bidang)
																<option value="{{  $bidang->id_bidang }}">{{ $bidang->nama_bidang }}</option>
																@endforeach
															</select>
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Email *</label>
															<input type="text" name="email" class="form-control" placeholder="Alamat Email">
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('jenis_pengaduan_id') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Jenis Pengaduan</label>
															<select name="jenis_pengaduan_id" class="form-control">
																@foreach($jenis_pengaduan as $jenis)
																<option value="{{  $jenis->id_jenis_pengaduan }}">{{ $jenis->jenis_pengaduan }}</option>
																@endforeach
															</select>
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('sumber_aduan_id') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Sumber Pengaduan</label>
															<select name="sumber_aduan_id" class="form-control">
																@foreach($sumber_pengaduan as $sumber)
																<option value="{{  $sumber->id_sumber_pengaduan }}">{{ $sumber->sumber_pengaduan }}</option>
																@endforeach
															</select>
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group {{ $errors->has('sumber_aduan_id') ? 'has-error' : '' }}">
															<label class="control-label mb-10">NIB</label>
															
															<input type="text" name="nib" class="form-control" placeholder="Nomor Induk Berusaha">
														</div>	
													</div>
													<div class="col-md-12">
														<div class="form-group {{ $errors->has('uraian_singkat') ? 'has-error' : '' }}">
															<label class="control-label mb-10">Uraian Singkat</label>
															<textarea rows="5" name="uraian" class="form-control" placeholder="Uraian Singkat Isi Pengaduan"></textarea>
														</div>	
													</div>
												</div>
												<div class="alert alert-success">
											<font color="white"><strong>Pengajuan pengaduan Anda akan menjalani proses verifikasi data terlebih dahulu, jika data yang Anda masukan valid, Anda akan mendapatkan kode melalui SMS / E-Mail yang Anda Inputkan.</strong> </font>
											</div>

											<div>
											<strong> <input name="checkbox" type="checkbox"  id="checkbox"  data-rule-required="true" /> &nbsp;Dengan ini saya menyatakan bahwa data yang saya kirim adalah benar dan valid. Saya bersedia menerima sanksi sesuai dengan ketentuan peraturan perundang-undang yang berlaku Jika data yang saya kirim tidak benar dan tidak valid.</strong> 
											</div>

											<div id="screen3"> 
												<div class="alert alert-success">
													<strong><font color="white" size="5"><i class="icon-question-sign"></i>&nbsp;Yakin data yang anda kirim sudah benar ??? </font></strong><hr />
											<!--<div class="form-actions">-->
													
													<button type="submit" class="btn btn-warning"><strong>Submit</strong></button>
											<!--onClick="doSimpan();"</div>-->
												</div>
											</div>
                                    
												
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
				
					<!-- Row -->
					<div class="row">
						<div class="col-md-6">
						</div>
						
						<div class="col-md-6">
						</div>
					</div>
					<!-- /Row -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
					<!-- /Row -->
					
				@endsection
@push('custom-scripts')
    <script language="javascript">

			$(function(){



			$("#screen3").hide();

			$('input[type="checkbox"]').click(function(){
			   if($(this).prop("checked") == true){
			            //    alert("Anda Setuju Dengan Memenuhi Syarat dan Ketentuan Berlaku");
							$("#screen3").show(500);
							$("#selanjutnya").show(500);
			            }
			            else if($(this).prop("checked") == false){
			              $("#screen3").hide(500);
						  $("#selanjutnya").hide(500);
			            }
			   
			  });
			  
			});


			</script> 
@endpush
