<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Pengaduan;
use App\Models\PengaduanH;
use App\Models\JenisPengaduan;
use App\Models\BidangPengaduan;
use App\Models\SumberPengaduan;
use App\Models\LampiranPengaduan;
use App\Http\Requests\sumber_pengaduan\StoreRequest;
use App\Http\Requests\sumber_pengaduan\UpdateRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Auth;

class SumberPengaduanController extends Controller
{
	
	//jenis pengaduan
	public function index(){

        $sumber_pengaduan = DB::table('sumber_pengaduan')
                ->orderBy('id_sumber_pengaduan', 'asc')
                ->get();

        return view('backend/sumber_pengaduan/index',['sumber_pengaduan' => $sumber_pengaduan]);

    }
	
	public function datatables(Datatables $datatables)
    {

        //$query = DB::select(DB::raw("select * from surat_pengaduan"));
		DB::statement(DB::raw('set @rownum=0'));
        
        $query = SumberPengaduan::select("sumber_pengaduan.*", DB::raw('@rownum  := @rownum  + 1 AS rownum'))
		->get();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }
	
	public function getActionButton($post)
    {
		$viewButton = '<a href="' . url('/sumber-pengaduan/ubah/' . $post['id_sumber_pengaduan']) . '" class="btn btn-info">Ubah</a>&nbsp';
		$viewButton .= '<button class="btn waves-effect waves-light btn-danger" data-button="delete-button" title="Hapus" data-id="' . $post['id_sumber_pengaduan'] . '">Hapus</i></button>&nbsp';
        
        return $viewButton;
    }
	
	public function destroy($id)
    {
        $sumberpengaduan = SumberPengaduan::find($id);

        if (empty($sumberpengaduan)) {
            return response(['meta' => 400, 'message' => 'Failed Delete Data'], 400);
        }

        $sumberpengaduan->delete();

        return response(['meta' => 200, 'message' => 'Success Delete Data'], 200);

    }
	
	public function ubah($id)
    {
        $sumber_pengaduan = SumberPengaduan::find($id);
		return view('backend/sumber_pengaduan/ubah',['sumber_pengaduan' => $sumber_pengaduan]);
    }
	
	public function tambah()
    {
        return view('backend/sumber_pengaduan/tambah');
    }
	
	public function create(StoreRequest $request)
    {
        $sumberpengaduan = new sumberPengaduan();
        $sumberpengaduan->sumber_pengaduan = $request->sumber_pengaduan;
        $sumberpengaduan->kode = $request->kode;

        $sumberpengaduan->save();

        return redirect('sumber-pengaduan')->with('saved', 'Data Berhasil Di Tambahkan');
    }
	
	public function update(UpdateRequest $request, $id)
    {
        $sumber_pengaduan = SumberPengaduan::FindOrFail($id);
        $sumber_pengaduan->sumber_pengaduan = $request->sumber_pengaduan;
        $sumber_pengaduan->kode = $request->kode;
        $sumber_pengaduan->update();

        return redirect('sumber-pengaduan')->with('saved', 'Data Berhasil Di Update');
    }
}
