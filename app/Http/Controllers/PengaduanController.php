<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Pengaduan;
use App\Models\PengaduanH;
use App\Models\JenisPengaduan;
use App\Models\LampiranPengaduan;
use App\Http\Requests\pengaduan\StoreRequest;
use App\Http\Requests\pengaduan\UpdateRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Auth;

class PengaduanController extends Controller
{
    public function indexgrafiks()
    {
        return view('backend/indexgrafik');
    }
    public function index()
    {
		$jenis_pengaduan = DB::table('jenis_pengaduan')
                ->orderBy('id_jenis_pengaduan', 'asc')
                ->get();

        $sumber_pengaduan = DB::table('sumber_pengaduan')
                ->orderBy('id_sumber_pengaduan', 'asc')
                ->get();

        $bidang_pengaduan = DB::table('bidang_pengaduan')
                ->orderBy('id_bidang', 'asc')
                ->get();

        return view('backend/index',['jenis_pengaduan' => $jenis_pengaduan,'sumber_pengaduan'=>$sumber_pengaduan,'bidang_pengaduan'=>$bidang_pengaduan]);

    }
    
    public function data_pengaduan()
    {
        return view('backend/pengaduan/data_pengaduan');
    }

    public function datatablesadd(Datatables $datatables, Request $request)
    {

        
        
    }

    public function addnew(Request $request)
    {
        $sendernumber = $request->sendernumber;
        $textdecoded = $request->textdecoded;
        $updatedindb = $request->updatedindb;
		
		$num = str_replace("+62", "0", $sendernumber);
		
        $PengaduanH = new PengaduanH();
        $PengaduanH->kode_pengaduan = "";
        $PengaduanH->nama_pengadu = "";
        $PengaduanH->instansi = 1;
        $PengaduanH->jabatan = "";
        $PengaduanH->alamat = "";
        $PengaduanH->nik = "";
        $PengaduanH->no_tlp = $num;
        $PengaduanH->email = "";
        $PengaduanH->jenis_pengaduan_id = 5;
        $PengaduanH->bidang_pengaduan_id = 1;
        $PengaduanH->uraian = $textdecoded;
        $PengaduanH->sumber_aduan_id = 2;
        $PengaduanH->proses_name = "verifikasi";
        $PengaduanH->acc = 0;
        $PengaduanH->status_flow = "Proses";
        $PengaduanH->api_key = $updatedindb;
        $PengaduanH->mulai = date("Y-m-d H:i:s");

        $PengaduanH->save();

        return redirect('pengaduan/api_sms')->with('saved', 'Data Berhasil Di Tambahkan');
    }
	
	public function addnewweb(Request $request)
    {
		$resi = $request->resi;
		$nama_pemohon = $request->nama_pemohon;
		$jabatan = $request->jabatan;
		$alamat_pemohon_jalan = $request->alamat_pemohon_jalan;
		$nik_pemohon = $request->nik_pemohon;
		$telp_pemohon = $request->telp_pemohon;
		$email_pemohon = $request->email_pemohon;
		$id_kategori_pengaduan = $request->id_kategori_pengaduan;
		$isi_pengaduan = $request->isi_pengaduan;
		
        $PengaduanH = new PengaduanH();
        $PengaduanH->kode_pengaduan = "";
        $PengaduanH->nama_pengadu = $nama_pemohon;
        $PengaduanH->instansi = 1;
        $PengaduanH->jabatan = $jabatan;
        $PengaduanH->alamat = $alamat_pemohon_jalan;
        $PengaduanH->nik = $nik_pemohon;
        $PengaduanH->no_tlp = $telp_pemohon;
        $PengaduanH->email = $email_pemohon;
        $PengaduanH->jenis_pengaduan_id = $id_kategori_pengaduan;
        $PengaduanH->uraian = $isi_pengaduan;
        $PengaduanH->sumber_aduan_id = 1;
        $PengaduanH->bidang_pengaduan_id = 1;
        $PengaduanH->api_key = $resi;
        $PengaduanH->proses_name = "verifikasi";
        $PengaduanH->acc = 0;
        $PengaduanH->mulai = date("Y-m-d H:i:s");
        $PengaduanH->status_flow = "Proses";

        $PengaduanH->save();

        return redirect('pengaduan/api_web')->with('saved', 'Data Berhasil Di Tambahkan');
    }

    public function getActionButtonAdd($post)
    {
        
    }

    public function datatables(Datatables $datatables)
    {

        //$query = DB::select(DB::raw("select * from surat_pengaduan"));
        
        $query = Pengaduan::all();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }

    public function getActionButton($post)
    {
        $viewButton = '<a href="' . url('/pengaduan/show/' . $post['id_surat_pengaduan']) . '" class="btn btn-sm btn-info" title="Detail">DETAIL</a>';
        
        return $viewButton;
    }

    public function create(Request $request)
    {
        
    }

    public function store(StoreRequest $request)
    {
        $PengaduanH = new PengaduanH();
        $PengaduanH->kode_pengaduan = "";
        $PengaduanH->nama_pengadu = $request->nama_pengadu;
        $PengaduanH->instansi = $request->instansi;
        $PengaduanH->jabatan = $request->jabatan;
        $PengaduanH->alamat = $request->alamat;
        $PengaduanH->nik = $request->nik;
        $PengaduanH->no_tlp = $request->no_tlp;
        $PengaduanH->email = $request->email;
        $PengaduanH->bidang_pengaduan_id = $request->bidang_pengaduan_id;
        $PengaduanH->jenis_pengaduan_id = $request->jenis_pengaduan_id;
        $PengaduanH->uraian = $request->uraian;
        $PengaduanH->sumber_aduan_id = $request->sumber_aduan_id;
        $PengaduanH->nib = $request->nib;
        $PengaduanH->proses_name = "verifikasi";
        $PengaduanH->mulai = date("Y-m-d H:i:s");
        $PengaduanH->acc = 0;
        $PengaduanH->status_flow = "Proses";

        $PengaduanH->save();

        return redirect('pengaduan')->with('saved', 'Data Berhasil Di Tambahkan');
        
    }

    public function show($id)
    {
		$pengaduan = Pengaduan::where('id_surat_pengaduan','=',$id)->first();

		//var_dump($pengaduan);die;
		
		$lampiran = Pengaduan::join('lampiran_pengaduan','lampiran_pengaduan.surat_pengaduan_id','=','surat_pengaduan.id_surat_pengaduan')->where('id_surat_pengaduan','=',$id)->get();
		
		return view('backend/pengaduan/detail_pengaduan', ['pengaduan' => $pengaduan, 'lampiran' => $lampiran]);
    }

    public function upload_lampiran(Request $request)
    {
        if($request->hasFile('photos')){	 
			$allowedfileExtension=['pdf'];	 
			$files = $request->file('photos');
			 
			foreach($files as $file){ 
				$filename = $file->getClientOriginalName();			 
				$extension = $file->getClientOriginalExtension();
				$check=in_array($extension,$allowedfileExtension);

				if($check){ 
					foreach ($request->photos as $photo) {	 
						$filename = $photo->store('photos'); 

						$LampiranPengaduan = new LampiranPengaduan();
						$LampiranPengaduan->surat_pengaduan_id = $request->surat_pengaduan_id;
						$LampiranPengaduan->filename = $filename;
						
						$LampiranPengaduan->save();
					}
					return redirect('pengaduan/show/'.$request->surat_pengaduan_id.'')->with('saved', 'Data Berhasil Di Upload');
				}else {
					return redirect('pengaduan/show/'.$request->surat_pengaduan_id.'')->with('failed', 'Gagal Upload, Perhatikan Format File');
				} 
			} 
		}
    }
	
	public function edit($id)
    {
        
    }

    public function getprogram(Request $request)
    {
        
    }

    public function update(UpdateRequest $request, $id)
    {
        
    }
	
    public function dataSms(){
    	$url = "http://perizinan-latihan.bandungbaratkab.go.id/pengaduan/sms";
		$z = file_get_contents($url);
		$a = json_decode($z,true);
		$bgz = "BuGrPIS6ptBW49WLOxPrxX1Roh";
		
		$data=openssl_decrypt($a,"AES-128-ECB",$bgz);
		
		$val = json_decode($data,true);
		
		foreach($val as $key => $value){
			$query = PengaduanH::where('api_key','=',$value['UpdatedInDB'])->get();
			if ($query->count() > 0 or $value['UpdatedInDB'] == ""){
				
			}else{
				$pengaduan[$key] = $value;
			}
		}
		
		//print_r($val);
		return view('backend/pengaduan/api_sms',['api_sms' => $pengaduan ]);
    }      
	
    public function dataPengaduan(){
    	$pengaduan = [];
		$url = "http://perizinan-latihan.bandungbaratkab.go.id/pengaduan";
		$z = file_get_contents($url);
		$a = json_decode($z,true);
		$bgz = "BuGrPIS6ptBW49WLOxPrxX1Roh";
		
		$data=openssl_decrypt($a,"AES-128-ECB",$bgz);
		
		$val = json_decode($data,true);
		
		foreach($val as $key => $value){
			$query = PengaduanH::where('api_key','=',$value['resi'])->get();
			if ($query->count() > 0 or $value['resi'] == ""){
				
			}else{
				$pengaduan[$key] = $value;
			}
		}
		
		//print_r($pengaduan);die;
		
		//die;
		return view('backend/pengaduan/api_web',['api_web' => $pengaduan, 'pengaduan' => $query ]);
    }
	
	public function insert_api_web($resi, $nama_pemohon,$jabatan,$alamat_pemohon_jalan,$nik_pemohon,$telp_pemohon,$email_pemohon,$id_kategori_pengaduan,$isi_pengaduan){
		
		$PengaduanH = new PengaduanH();
        $PengaduanH->kode_pengaduan = "";
        $PengaduanH->nama_pengadu = $nama_pemohon;
        $PengaduanH->instansi = 1;
        $PengaduanH->jabatan = $jabatan;
        $PengaduanH->alamat = $alamat_pemohon_jalan;
        $PengaduanH->nik = $nik_pemohon;
        $PengaduanH->no_tlp = $telp_pemohon;
        $PengaduanH->email = $email_pemohon;
        $PengaduanH->jenis_pengaduan_id = $id_kategori_pengaduan;
        $PengaduanH->uraian = $isi_pengaduan;
        $PengaduanH->sumber_aduan_id = 1;

        $PengaduanH->bidang_pengaduan_id = 1;
        $PengaduanH->api_key = $resi;

        $PengaduanH->proses_name = "verifikasi";
        $PengaduanH->acc = 0;
        $PengaduanH->status_flow = "Proses";

        $PengaduanH->save();

        return redirect('pengaduan/api_web')->with('saved', 'Data Berhasil Di Tambahkan');
	}
	
	public function insert_api_sms($sendernumber, $text,$updatedindb){
		
        $num = str_replace("+62", "0", $sendernumber);
		$t = htmlspecialchars($text);
		
        $PengaduanH = new PengaduanH();
        $PengaduanH->kode_pengaduan = "";
        $PengaduanH->nama_pengadu = "";
        $PengaduanH->instansi = 1;
        $PengaduanH->jabatan = "";
        $PengaduanH->alamat = "";
        $PengaduanH->nik = "";
        $PengaduanH->no_tlp = $num;
        $PengaduanH->email = "";
        $PengaduanH->jenis_pengaduan_id = 5;
        $PengaduanH->bidang_pengaduan_id = 1;
        $PengaduanH->uraian = $t;
        $PengaduanH->sumber_aduan_id = 2;
        $PengaduanH->proses_name = "verifikasi";
        $PengaduanH->acc = 0;
        $PengaduanH->status_flow = "Proses";
        $PengaduanH->api_key = $updatedindb;

        $PengaduanH->save();

        return redirect('pengaduan/api_sms')->with('saved', 'Data Berhasil Di Tambahkan');
	}
	
	//jenis pengaduan
	public function jenis_pengaduan(){

        $jenis_pengaduan = DB::table('jenis_pengaduan')
                ->orderBy('id_jenis_pengaduan', 'asc')
                ->get();

        return view('backend/pengaduan/jenis_pengaduan',['jenis_pengaduan' => $jenis_pengaduan]);

    }
	
	public function datatables_jenis(Datatables $datatables)
    {

        //$query = DB::select(DB::raw("select * from surat_pengaduan"));
        
        $query = JenisPengaduan::all();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButtonJenis($post);
        });

        return $datatables->make(true);
    }
	
	public function getActionButtonJenis($post)
    {
		$viewButton = '<a href="' . url('/pengaduan/ubah_jenis/' . $post['id_jenis_pengaduan']) . '" class="btn btn-info">Ubah</a>&nbsp';
		$viewButton .= '<button class="btn waves-effect waves-light btn-danger" data-button="delete-button" title="Hapus" data-id="' . $post['id_jenis_pengaduan'] . '">Hapus</i></button>&nbsp';
        
        return $viewButton;
    }
	
	public function delete_jenis($id)
    {
        $jenispengaduan = JenisPengaduan::find($id);

        if (empty($jenispengaduan)) {
            return response(['meta' => 400, 'message' => 'Failed Delete Data'], 400);
        }

        $jenispengaduan->delete();

        return response(['meta' => 200, 'message' => 'Success Delete Data'], 200);

    }
	
	public function ubah_jenis($id)
    {
        echo "ok";
    }
}
