<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BidangPengaduan extends Model
{
    protected $table = 'bidang_pengaduan';
    protected $primaryKey = 'id_bidang';
    protected $fillable = [
	
		'nama_bidang',
    ];
}
