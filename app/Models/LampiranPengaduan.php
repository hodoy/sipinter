<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LampiranPengaduan extends Model
{
    protected $table = 'lampiran_pengaduan';
    protected $primaryKey = 'id_lampiran_pengaduan';
    protected $fillable = [
    	'surat_pengaduan_id',
    	'filename',
    ];
}
