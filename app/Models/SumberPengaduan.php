<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SumberPengaduan extends Model
{
    protected $table = 'sumber_pengaduan';
    protected $primaryKey = 'id_sumber_pengaduan';
    protected $fillable = [
	
		'sumber_pengaduan',
		'kode',
    ];
}
