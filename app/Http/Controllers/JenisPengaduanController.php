<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Pengaduan;
use App\Models\PengaduanH;
use App\Models\JenisPengaduan;
use App\Models\LampiranPengaduan;
use App\Http\Requests\jenis_pengaduan\StoreRequest;
use App\Http\Requests\jenis_pengaduan\UpdateRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Auth;

class JenisPengaduanController extends Controller
{
	
	//jenis pengaduan
	public function index(){

        $jenis_pengaduan = DB::table('jenis_pengaduan')
                ->orderBy('id_jenis_pengaduan', 'asc')
                ->get();

        return view('backend/jenis_pengaduan/index',['jenis_pengaduan' => $jenis_pengaduan]);

    }
	
	public function datatables(Datatables $datatables)
    {

        DB::statement(DB::raw('set @rownum=0'));
        
        $query = JenisPengaduan::select("jenis_pengaduan.*", DB::raw('@rownum  := @rownum  + 1 AS rownum'))
		->get();

        $datatables = Datatables::of($query);

        $datatables->addColumn('action', function ($post) {
            return $this->getActionButton($post);
        });

        return $datatables->make(true);
    }
	
	public function getActionButton($post)
    {
		$viewButton = '<a href="' . url('/jenis-pengaduan/ubah_jenis/' . $post['id_jenis_pengaduan']) . '" class="btn btn-info">Ubah</a>&nbsp';
		$viewButton .= '<button class="btn waves-effect waves-light btn-danger" data-button="delete-button" title="Hapus" data-id="' . $post['id_jenis_pengaduan'] . '">Hapus</i></button>&nbsp';
        
        return $viewButton;
    }
	
	public function destroy($id)
    {
        $jenispengaduan = JenisPengaduan::find($id);

        if (empty($jenispengaduan)) {
            return response(['meta' => 400, 'message' => 'Failed Delete Data'], 400);
        }

        $jenispengaduan->delete();

        return response(['meta' => 200, 'message' => 'Success Delete Data'], 200);

    }
	
	public function ubah_jenis($id)
    {
        $jenis_pengaduan = JenisPengaduan::find($id);
		return view('backend/jenis_pengaduan/ubah',['jenis_pengaduan' => $jenis_pengaduan]);
    }
	
	public function tambah()
    {
        return view('backend/jenis_pengaduan/tambah');
    }
	
	public function create(StoreRequest $request)
    {
        $jenispengaduan = new JenisPengaduan();
        $jenispengaduan->jenis_pengaduan = $request->jenis_pengaduan;

        $jenispengaduan->save();

        return redirect('jenis-pengaduan')->with('saved', 'Data Berhasil Di Tambahkan');
    }
	
	public function update(UpdateRequest $request, $id)
    {
        $jenis_pengaduan = JenisPengaduan::FindOrFail($id);
        $jenis_pengaduan->jenis_pengaduan = $request->jenis_pengaduan;
        $jenis_pengaduan->update();

        return redirect('jenis-pengaduan')->with('saved', 'Data Berhasil Di Update');
    }
}
