@extends('backend.main')
@section('content')
	<div class="container">
		<style type="text/css">
			#ew{
				padding: 1px 1px 1px 1px;
				border : 1px solid;
				border-color: skyblue;
			}
		</style>
					<script src="https://code.highcharts.com/highcharts.js"></script>
					<script src="https://code.highcharts.com/modules/series-label.js"></script>
					<script src="https://code.highcharts.com/modules/exporting.js"></script>
					<script src="https://code.highcharts.com/modules/export-data.js"></script>
					<!-- Title -->
					
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
					<div class="col-sm-12">

					<div class="col-sm-8 col-xs-12">
								<div class="panel panel-default border-panel card-view panel-refresh">
									<div class="refresh-container">
										<div class="la-anim-1"></div>
									</div>
									<div class="panel-heading">
										<div class="pull-left">
											<h6 class="panel-title txt-dark">STATISTIK PENGADUAN 1 BULAN TERAKHIR</h6>
										</div>
										<div class="pull-right">
											<a href="#" class="pull-left inline-block refresh mr-15">
												<i class="zmdi zmdi-replay"></i>
											</a>
											<a href="#" class="pull-left inline-block full-screen mr-15">
												<i class="zmdi zmdi-fullscreen"></i>
											</a>
											<div class="pull-left inline-block dropdown">
												<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>
												<ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
													<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Devices</a></li>
													<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>General</a></li>
													<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>Referral</a></li>
												</ul>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
											<div id="containerz"></div>
											
										</div>
									</div>
								</div>
							</div>
							

							<div class="col-sm-4 col-xs-12">
								<div class="panel panel-default border-panel card-view panel-refresh">
									<div class="refresh-container">
										<div class="la-anim-1"></div>
									</div>
									<div class="panel-heading">
										<div class="pull-left">
											<h6 class="panel-title txt-dark"><i class="fa fa-group"></i> PENGADUAN MASUK</h6>
										</div>
										
										<div class="clearfix"></div>
									</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
											
												914,001
												visits
											
														
											
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-4 col-xs-12">
								<div class="panel panel-default border-panel card-view panel-refresh">
									<div class="refresh-container">
										<div class="la-anim-1"></div>
									</div>
									<div class="panel-heading">
										<div class="pull-left">
											<h6 class="panel-title txt-dark"><i class="fa fa-send"></i> PENGADUAN DALAM PROSES</h6>
										</div>
										
										<div class="clearfix"></div>
									</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
												914,001
												visits
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-sm-4 col-xs-12">
								<div class="panel panel-default border-panel card-view panel-refresh">
									<div class="refresh-container">
										<div class="la-anim-1"></div>
									</div>
									<div class="panel-heading">
										<div class="pull-left">
											<h6 class="panel-title txt-dark"><i class="fa fa-line-chart"></i> PENGADUAN SELESAI TANGANI</h6>
										</div>
										
										<div class="clearfix"></div>
									</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
											914,001
											visits
											
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-4 col-xs-12">
								<div class="panel panel-default border-panel card-view panel-refresh">
									<div class="refresh-container">
										<div class="la-anim-1"></div>
									</div>
									<div class="panel-heading">
										<div class="pull-left">
											<h6 class="panel-title txt-dark"><i class="fa fa-pie-chart"></i> PERSENTASE PENANGANAN</h6>
										</div>
										
										<div class="clearfix"></div>
									</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
											914,001
											visits
											
										</div>
									</div>
								</div>
							</div>					

						</div>
					</div>
					
				@endsection
				@push('custom-scripts')
<script type="text/javascript">
	
Highcharts.chart('containerz', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'JUMLAH PENGADUAN BERDASARKAN SUMBER PENGADUAN'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: <?php echo $date; ?>,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'JUMLAH PENGADUAN'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: <?php echo $dataa;?>
});
</script>
@endpush
