<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengaduanH extends Model
{

    protected $table = 'pengaduan';
    protected $primaryKey = 'id';
    protected $fillable = [
	
		'kode',
    	'nama_pengadu',
    	'instansi',
    	'jabatan',
    	'alamat',
    	'nik',
    	'no_tlp',
    	'email',
    	'jenis_pengaduan_id',
    	'uraian',
    	'acc',
        'proses_name',
        'sumber_aduan_id',
        'status_flow',
        'bidang_pengaduan_id',
        'api_key',
        'nib',
        'mulai',
    ];
}
