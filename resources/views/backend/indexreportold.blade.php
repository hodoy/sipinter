@extends('backend.main')
@section('content')
<div class="container">
					
					<!-- Title -->
					<br>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div>
										<center><h3 class="panel-title txt-dark">LAPORAN PENGADUAN</h3></center>
									</div>
									
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										
										<div class="form-wrap mt-40">
												
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label mb-10">Sumber Pengaduan</label> 
															<select class="form-control" name='jenis'>
									                            <option value="0">Semua Sumber Pengaduan</option>
									                            @foreach($jenis as $jen)
									                            	<option value="{{$jen->id_sumber_pengaduan}}">{{$jen->sumber_pengaduan}}</option>
									                            @endforeach
									                        </select>
														</div>	
													</div>
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label mb-10">Periode Awal *</label> 
															<input type="text" name="awal" class="form-control" placeholder="yyyy-mm-dd">
														</div>	
													</div>
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label mb-10">Periode Akhir *</label> 
															<input type="text" name="akhir" class="form-control" placeholder="yyyy-mm-dd">
														</div>	
													</div>
													<center>
													<button type="submit" class="btn btn-success" id="btn-monitor">MUAT</button>
													</center>
												
											</div>
                                    
												
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
				
		

@endsection

@push('custom-scripts')
  <script>
        $(function () {
           
			function checkData(){
                var awal  = $('input[name="awal"]').val()
                var akhir  = $('input[name="akhir"]').val()
                var type  = $('select[name="jenis"]').val()
                 if(awal == '' || akhir ==''){
                    alert('Silahkan Pilih Periode Awal Dan Akhir Terlebih Dahulu !!!');
                }else{
          			window.location = "{!!URL::to('/reporthasil')!!}" + "?a1=" + awal + "&a2=" +akhir + "&type=" + type ;
          		}
                
            }
            
            $('#btn-monitor').click(function(){
                checkData();
            });
            
        });


    </script>
@endpush