<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisPengaduan extends Model
{
    protected $table = 'jenis_pengaduan';
    protected $primaryKey = 'id_jenis_pengaduan';
    protected $fillable = [
	
		'jenis_pengaduan',
    ];
}
