<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Models\PengaduanH;
use App\Models\JenisPengaduan;
use App\Models\SumberPengaduan;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index()
    {
    	$jenis = SumberPengaduan::all();
        return view('backend/indexreport',['jenis'=>$jenis]);
    }
    public function report(Request $request)
    {
//    	$users = DB::connection('mysql2')->table('pengaduan')->get();
    	$type = $request->type;
    	$a1 = $request->a1;
    	$a2 = $request->a2;

    	if ($type == 0) {
    		$sumber_pengaduan = "Semua Sumber Pengaduan";
    		$data = PengaduanH::join('sumber_pengaduan','pengaduan.sumber_aduan_id','=','sumber_pengaduan.id_sumber_pengaduan')->join('jenis_pengaduan','pengaduan.jenis_pengaduan_id','=','jenis_pengaduan.id_jenis_pengaduan')->whereBetween('pengaduan.mulai', array($a1,$a2))->orderBy('pengaduan.status_flow','desc')->get();

        $pdf = PDF::loadView('report/index', ['data' => $data,'d1'=>$a1,'d2'=>$a2,'type'=>$type,'sumber'=>$sumber_pengaduan])->setPaper('f4', 'potrait');
        
        return $pdf->stream('report.pdf');	
    	}if ($type != 0){
    		$sumber_pengaduan = SumberPengaduan::select('sumber_pengaduan')->where('id_sumber_pengaduan','=',$type)->first();
    		$data = PengaduanH::join('sumber_pengaduan','pengaduan.sumber_aduan_id','=','sumber_pengaduan.id_sumber_pengaduan')->join('jenis_pengaduan','pengaduan.jenis_pengaduan_id','=','jenis_pengaduan.id_jenis_pengaduan')->where('sumber_pengaduan.id_sumber_pengaduan','=',$type)->whereBetween('pengaduan.mulai', array($a1,$a2))->orderBy('pengaduan.status_flow','desc')->get();

        $pdf = PDF::loadView('report/index', ['data' => $data,'d1'=>$a1,'d2'=>$a2,'type'=>$type,'sumber'=>$sumber_pengaduan])->setPaper('f4', 'potrait');
        
        return $pdf->stream('report.pdf');
    	}

  		
    }
}
