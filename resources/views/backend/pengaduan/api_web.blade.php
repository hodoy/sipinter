@extends('backend.main')
@section('content')
	<div class="container">

					<br>
					
					<!-- /Title -->

					<!-- Row -->
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">DATA DARI WEB PENGADUAN</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="table-wrap">
											<div class="table-responsive">
											@if (session('saved'))
											<div class="alert alert-success">
											  <strong>Success!</strong>{{ session('saved') }}
												  <button type="button" class="close" data-dismiss="alert">x</button>
											</div>
											@endif
												<table id="datable_1" class="table table-hover display  pb-30" data-tables="true">
												<thead>
													<tr>
														<th>No</th>
														<th>Tgl</th>
														<th>Nama</th>
														<th>Isi Pengaduan</th>
														<th>Email</th>
														<th>Telp</th>
														<th>Resi</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												<?php $no=1; ?>
													@foreach($api_web as $api)
														<?php 
															$resi = $api['resi']; 
															$nama_pemohon = $api['nama_pemohon']; 
															$jabatan = $api['jabatan']; 
															$alamat_pemohon_jalan = $api['alamat_pemohon_jalan']; 
															$nik_pemohon = $api['nik_pemohon']; 
															$telp_pemohon = $api['telp_pemohon']; 
															$email_pemohon = $api['email_pemohon']; 
															$id_kategori_pengaduan = $api['id_kategori_pengaduan']; 
															$isi_pengaduan = $api['isi_pengaduan']; 
														?>
															<tr>
																<td>{{ $no++ }}</td>
																<td>{{ $api['tgl_pengaduan'] }}</td>
																<td>{{ $api['nama_pemohon'] }}</td>
																<td>{{ $api['isi_pengaduan'] }}</td>
																<td>{{ $api['email_pemohon'] }}</td>
																<td>{{ $api['telp_pemohon'] }}</td>
																<td>{{ $api['resi'] }}</td>
																<td>
																
																<form action="{{ url('insert_apiweb') }}" method="post">
																{{csrf_field()}}
																	<input type="hidden" name="resi" value="{{ $resi }}">
																	<input type="hidden" name="nama_pemohon" value="{{ $nama_pemohon }}">
																	<input type="hidden" name="jabatan" value="{{ $jabatan }}">
																	<input type="hidden" name="alamat_pemohon_jalan" value="{{ $alamat_pemohon_jalan }}">
																	<input type="hidden" name="nik_pemohon" value="{{ $nik_pemohon }}">
																	<input type="hidden" name="telp_pemohon" value="{{ $telp_pemohon }}">
																	<input type="hidden" name="email_pemohon" value="{{ $email_pemohon }}">
																	<input type="hidden" name="id_kategori_pengaduan" value="{{ $id_kategori_pengaduan }}">
																	<input type="hidden" name="isi_pengaduan" value="{{ $isi_pengaduan }}">
																	<button type="submit" onclick="return confirm('Apakah Yakin ??')" class="btn btn-info">PROSES</button>
																</form>
																
																</td>
															</tr>
													@endforeach
											</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-6">
						</div>

						<div class="col-md-6">
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
					<!-- /Row -->

				@endsection
