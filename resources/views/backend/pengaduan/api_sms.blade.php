@extends('backend.main')
@section('content')
	<div class="container">

					<br>
					
					<!-- /Title -->

					<!-- Row -->
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default border-panel card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">DATA DARI SMS PENGADUAN</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="table-wrap">
											<div class="table-responsive">
												<table id="datable_1" class="table table-hover display  pb-30" data-tables="true">
												<thead>
													<tr>
														<th>No</th>
														<th>Nomor</th>
														<th>Isi Pesan</th>
														<th>Waktu</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												<?php $no=1 ?>
												@foreach($api_sms as $api)
												
												<?php
													$sendernumber = $api['SenderNumber']; 
													$textdecoded = $api['TextDecoded']; 
													$textdecoded2 = "coba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba cobacoba coba coba"; 
													$text = htmlspecialchars($textdecoded);
													$updatedindb = $api['UpdatedInDB'];													
												?>
													<tr>
														<td>{{ $no++ }}</td>
														<td>{{ $api['SenderNumber'] }}</td>
														<td>{{ $api['TextDecoded'] }}</td>
														<td>{{ $api['UpdatedInDB'] }}</td>
														<td>
														
														<form action="{{ url('insert_apisms') }}" method="post">
														{{csrf_field()}}
															<input type="hidden" name="sendernumber" value="{{ $sendernumber }}">
															<input type="hidden" name="textdecoded" value="{{ $textdecoded }}">
															<input type="hidden" name="updatedindb" value="{{ $updatedindb }}">
															<button type="submit" onclick="return confirm('Apakah Yakin ??')" class="btn btn-info">PROSES</button>
														</form>
														
														</td>
													</tr>
												@endforeach
											</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-6">
						</div>

						<div class="col-md-6">
						</div>
					</div>
					<!-- /Row -->

					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
					<!-- /Row -->

				@endsection